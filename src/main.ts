import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import { Input, Button, Row, Col, Link, Loading } from 'element-ui';
import { LMap, LTileLayer, LMarker, LPopup } from 'vue2-leaflet';

import 'element-ui/lib/theme-chalk/index.css';
import 'leaflet/dist/leaflet.css';

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-popup', LPopup);

Vue.use(Input);
Vue.use(Button);
Vue.use(Row);
Vue.use(Col);
Vue.use(Link);
Vue.use(Loading);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
