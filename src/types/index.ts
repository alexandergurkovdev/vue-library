export interface Libraries {
    _id: string;
    hash: string;
    nativeId: string;
    activated: string;
    data: {
        info: {
            path: string;
            category: string;
            createDate: string;
            updateDate: string;
        };
        general: {
            id: number;
            name: string;
            description: string;
            status: string;
            address: {
                street: string;
                comment: string;
                fiasHouseId: string;
                fiasStreetId: string;
                fiasCityId: string;
                fiasSettlementId: string;
                fiasRegionId: string;
                fullAddress: string;
                mapPosition: { type: string; coordinates: Array<any> };
            };
            category: { name: string; sysName: string };
            contacts: { website: string; phones: [{ value: string; comment: string }] };
            externalInfo: [
                {
                    url: string;
                    serviceName: string;
                }
            ];
            image: {
                url: string;
                title: string;
            };
            localeIds: Array<any>;
            locale: { name: string; timezone: string; sysName: string; id: number };
            organization: {
                id: number;
                name: string;
                inn: string;
                type: string;
                address: {
                    street: string;
                    fiasCityId: string;
                    fiasRegionId: string;
                    fullAddress: string;
                };
                subordinationIds: Array<any>;
                subordination: { name: string; timezone: string; sysName: string; id: number };
                localeIds: Array<any>;
                locale: { name: string; timezone: string; sysName: string; id: number };
            };
            workingSchedule: {
                key: { from: string; to: string };
            };
            ticketReport: Array<any>;
        };
    };
    status: number;
    errorFields: [
        {
            keyword: string;
            dataPath: string;
            schemaPath: string;
            params: { type: string };
            message: string;
        }
    ];
    updateSession: string;
    odSetVersions: Array<any>;
    odSchema: string;
    dataset: string;
    nativeName: string;
    created: string;
    modified: string;
}
