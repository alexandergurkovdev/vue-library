export default [
    {
        _id: '5da833d7628b0e30ade6b2c6',
        hash: '2019-10-15T12:31:45Z',
        nativeId: '40068',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40068',
                category: 'place',
                createDate: '2019-10-10T11:49:00Z',
                updateDate: '2019-10-15T12:31:45Z'
            },
            general: {
                id: 40068,
                name: 'Библиотека № 14 с. Кемуль',
                description:
                    '<p>Библиотека основана 4 мая 1925 года. </p><p>Фонд библиотеки – 7 219 книг и журналов. Количество посещений за год – 5 078 человек. Библиотека обслуживает всё население села.</p><p>Основное направление деятельности филиала – краеведение. За многие годы собран богатый материал по истории села Кемуль, основанный на данных из архивов, воспоминаниях старожилов села. Оформлен альбом «История с. Кемуль», создана летопись «Кто мы, откуда мы», проходят обсуждения книг и периодических изданий по истории села. Также оформляются книжные выставки, проводятся мероприятия для детей с играми и викторинами. Партнерами библиотеки являются: детский сад, школа, клуб.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Комсомольская,д 10',
                    comment: '',
                    fiasHouseId: '705df1e3-8db6-4b46-a452-bc627e90c854',
                    fiasStreetId: 'f101cca6-e91f-414b-8212-cb560a04f3d8',
                    fiasCityId: '06d7a6d6-8f57-4e5a-b698-2bc44c92bb84',
                    fiasSettlementId: '73b3e31b-747e-458c-9c1b-12c8298c4795',
                    fiasRegionId: '4f8b1a21-e4bb-422f-9087-d3cbf4bebc14',
                    fullAddress: 'край Пермский,г Чайковский,с Кемуль,ул Комсомольская,д 10',
                    mapPosition: { type: 'Point', coordinates: [53.9409613609314, 56.670934251302704] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: {
                    website: 'http://chaiklib.permculture.ru',
                    email: 'gydkova.olga1987god@mail.ru',
                    phones: [{ value: '73424144545', comment: '' }]
                },
                externalInfo: [
                    { url: 'https://www.culture.ru/institutes/40840/biblioteka-14-s-kemul', serviceName: 'Культура.рф' }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/a98430fe4e6b22296d69d616c43e0ce3.jpeg',
                    title: 'Кемуль.jpg'
                },
                localeIds: [1561, 209, 1],
                locale: { name: 'Чайковский', timezone: 'Asia/Yekaterinburg', sysName: 'chaykovskiy', id: 1561 },
                organization: {
                    id: 10482,
                    name: 'МБУК «Чайковская централизованная библиотечная система»',
                    inn: '5920010312',
                    type: 'mincult',
                    address: {
                        street: 'ул Кабалевского,д 28',
                        comment: '',
                        fiasHouseId: 'd6d523e0-ec90-450c-bc00-486719fbec96',
                        fiasStreetId: '4aaf7e6f-00ca-4a06-8ac7-cac02b9f7986',
                        fiasCityId: '06d7a6d6-8f57-4e5a-b698-2bc44c92bb84',
                        fiasRegionId: '4f8b1a21-e4bb-422f-9087-d3cbf4bebc14',
                        fullAddress: 'край Пермский,г Чайковский,ул Кабалевского,д 28'
                    },
                    subordinationIds: [1561, 209, 1],
                    subordination: {
                        name: 'Чайковский',
                        timezone: 'Asia/Yekaterinburg',
                        sysName: 'chaykovskiy',
                        id: 1561
                    },
                    localeIds: [1561, 209, 1],
                    locale: { name: 'Чайковский', timezone: 'Asia/Yekaterinburg', sysName: 'chaykovskiy', id: 1561 },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Центральная библиотека г. Чайковский',
                            networkId: '187559405',
                            updateDate: '2019-10-13T15:27:01Z',
                            createDate: '2019-10-13T15:27:01Z',
                            accountId: 27099,
                            postingGroupId: 24247
                        },
                        {
                            network: 'vk',
                            name: 'Чайковский читающий',
                            networkId: '187561185',
                            updateDate: '2019-10-13T16:15:16Z',
                            createDate: '2019-10-13T16:15:16Z',
                            accountId: 27099,
                            postingGroupId: 24248
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' },
                    { id: 406, name: 'Краеведение', sysName: 'kraevedenie' }
                ],
                workingSchedule: {
                    '1': { from: '08:30:00', to: '16:30:00' },
                    '3': { from: '08:30:00', to: '16:30:00' },
                    '5': { from: '09:00:00', to: '13:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека № 14 с. Кемуль',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.870Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2c7',
        hash: '2019-10-15T12:14:59Z',
        nativeId: '40067',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40067',
                category: 'place',
                createDate: '2019-10-10T11:20:43Z',
                updateDate: '2019-10-15T12:14:59Z'
            },
            general: {
                id: 40067,
                name: 'Библиотека № 16 с. Уральского',
                description:
                    '<p>Библиотека основана в 1909 году. Фонд насчитывает 9 360 книг и журналов. Посещений за год – 5 268. Библиотека обслуживает всё население Уральского территориального отдела.</p><p>В библиотеке для школьников проводятся познавательные мероприятия с театрализованным разыгрыванием отрывков из произведений литературы. Организован кружок «Мастерилка», где ребята +изготовляют поделки по схемам из книг и журналов. Регулярно проводятся выставки, приуроченные к интересным датам из календаря.</p><p>Библиотека сотрудничает с детским садом, школой, клубом. Частыми гостями библиотеки являются творческие люди, жители села и города Чайковский.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Школьная,д 3',
                    comment: '',
                    fiasHouseId: '22c7d0a7-a6f5-442c-9c14-5975e149bbf0',
                    fiasStreetId: 'e025ed94-970b-4b8c-a017-70b770669133',
                    fiasCityId: '06d7a6d6-8f57-4e5a-b698-2bc44c92bb84',
                    fiasSettlementId: 'cd9f3149-7a03-4bd3-b1f4-2cad334fe673',
                    fiasRegionId: '4f8b1a21-e4bb-422f-9087-d3cbf4bebc14',
                    fullAddress: 'край Пермский,г Чайковский,с Уральское,ул Школьная,д 3',
                    mapPosition: { type: 'Point', coordinates: [54.39288675785064, 56.44075702702873] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: {
                    website: 'http://chaiklib.permculture.ru',
                    email: 'gaynetdinova.y@mail.ru',
                    phones: [{ value: '73424156120', comment: '' }]
                },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40841/biblioteka-16-s-uralskogo',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    { url: 'https://all.culture.ru/uploads/bcb60d7eaea2599bf4a52231e1b9f016.jpeg', title: '3.jpg' },
                    { url: 'https://all.culture.ru/uploads/4fa04ade89801d32a53e68a92ef232f8.jpeg', title: '5.jpg' }
                ],
                image: { url: 'https://all.culture.ru/uploads/44434fbca58590f9873f1218f3f80321.jpeg', title: '1.jpg' },
                localeIds: [1561, 209, 1],
                locale: { name: 'Чайковский', timezone: 'Asia/Yekaterinburg', sysName: 'chaykovskiy', id: 1561 },
                organization: {
                    id: 10482,
                    name: 'МБУК «Чайковская централизованная библиотечная система»',
                    inn: '5920010312',
                    type: 'mincult',
                    address: {
                        street: 'ул Кабалевского,д 28',
                        comment: '',
                        fiasHouseId: 'd6d523e0-ec90-450c-bc00-486719fbec96',
                        fiasStreetId: '4aaf7e6f-00ca-4a06-8ac7-cac02b9f7986',
                        fiasCityId: '06d7a6d6-8f57-4e5a-b698-2bc44c92bb84',
                        fiasRegionId: '4f8b1a21-e4bb-422f-9087-d3cbf4bebc14',
                        fullAddress: 'край Пермский,г Чайковский,ул Кабалевского,д 28'
                    },
                    subordinationIds: [1561, 209, 1],
                    subordination: {
                        name: 'Чайковский',
                        timezone: 'Asia/Yekaterinburg',
                        sysName: 'chaykovskiy',
                        id: 1561
                    },
                    localeIds: [1561, 209, 1],
                    locale: { name: 'Чайковский', timezone: 'Asia/Yekaterinburg', sysName: 'chaykovskiy', id: 1561 },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Центральная библиотека г. Чайковский',
                            networkId: '187559405',
                            updateDate: '2019-10-13T15:27:01Z',
                            createDate: '2019-10-13T15:27:01Z',
                            accountId: 27099,
                            postingGroupId: 24247
                        },
                        {
                            network: 'vk',
                            name: 'Чайковский читающий',
                            networkId: '187561185',
                            updateDate: '2019-10-13T16:15:16Z',
                            createDate: '2019-10-13T16:15:16Z',
                            accountId: 27099,
                            postingGroupId: 24248
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '11:15:00', to: '18:00:00' },
                    '2': { from: '11:15:00', to: '18:00:00' },
                    '3': { from: '11:15:00', to: '18:00:00' },
                    '4': { from: '11:15:00', to: '18:00:00' },
                    '5': { from: '11:15:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека № 16 с. Уральского',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.872Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2c8',
        hash: '2019-10-14T13:38:43Z',
        nativeId: '40060',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40060',
                category: 'place',
                createDate: '2019-10-10T05:31:26Z',
                updateDate: '2019-10-14T13:38:43Z'
            },
            general: {
                id: 40060,
                name: 'Рогато-Балковская библиотека',
                description:
                    '<p>Рогато-Балковская библиотека была основана в 1932 году. До сегодняшнего дня претерпевала изменения от избы-читальни до МКУК ПЦБС Рогато-Балковский филиал № 12. </p><p>Фонд содержит более 20 тыс. единиц отечественной и зарубежной литературы. </p><p>В библиотеке действует два клуба: «Поговорим по душам» для читателей пенсионного возраста и клуб «Золотой ключик» для читателей среднего школьного возраста. </p><p>Филиал осуществляет совместную работу с другими учреждениями: СОШ № 16, ДК поселка, детским садом № 5 «Чебурашка», территориальным отделом п. Рогатая Балка, ЦСОН (Центр социального обслуживания населения) поселка.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Первомайская,д 75',
                    comment: '',
                    fiasStreetId: '50284e22-81d2-4fdb-8b53-8087ab1c8e1b',
                    fiasSettlementId: 'd8f7fef6-ff39-41bd-87e2-0d3a1ab48f33',
                    fiasAreaId: '74275c7d-b864-4048-b9bc-139c58331098',
                    fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                    fullAddress: 'край Ставропольский,р-н Петровский,п Рогатая Балка,ул Первомайская,д 75',
                    mapPosition: { type: 'Point', coordinates: [43.03845226764679, 45.2073648764861] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [{ value: '78654765191', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40832/rogato-balkovskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/9088690aa368801bac03e44eb3ef7e1f.jpeg',
                    title: 'a3c722d9ef31e87057eb8f81be208c65.jpg'
                },
                localeIds: [2702, 176, 1],
                locale: { name: 'Петровский район', timezone: 'Europe/Moscow', sysName: 'petrovskiy-rayon', id: 2702 },
                organization: {
                    id: 12445,
                    name: 'МКУК «Петровская централизованная библиотечная система»',
                    inn: '2617008469',
                    type: 'mincult',
                    address: {
                        street: 'пл 50 лет Октября,д 10',
                        comment: '',
                        fiasStreetId: '8dd5dbe4-ed08-408b-85d7-7b5faa0fa026',
                        fiasCityId: '019979ae-6056-4ed8-98ef-48bded318dd2',
                        fiasAreaId: '74275c7d-b864-4048-b9bc-139c58331098',
                        fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                        fullAddress: 'край Ставропольский,р-н Петровский,г Светлоград,пл 50 лет Октября,д 10'
                    },
                    subordinationIds: [2702, 176, 1],
                    subordination: {
                        name: 'Петровский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petrovskiy-rayon',
                        id: 2702
                    },
                    localeIds: [2702, 176, 1],
                    locale: {
                        name: 'Петровский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petrovskiy-rayon',
                        id: 2702
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Петровская центральная библиотека',
                            networkId: '98662292',
                            updateDate: '2016-02-03T13:49:13Z',
                            createDate: '2016-02-03T13:49:13Z',
                            accountId: 5075,
                            postingGroupId: 4558
                        },
                        {
                            network: 'ok',
                            name: 'Петровская центральная библиотека',
                            networkId: '51633647124623',
                            updateDate: '2016-02-03T13:48:25Z',
                            createDate: '2016-02-03T13:48:25Z',
                            accountId: 5074,
                            postingGroupId: 4557
                        }
                    ]
                },
                recommendations: [
                    { name: 'Петровский район', timezone: 'Europe/Moscow', sysName: 'petrovskiy-rayon', id: 2702 }
                ],
                tags: [
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '0': { from: '08:00:00', to: '17:00:00' },
                    '1': { from: '08:00:00', to: '17:00:00' },
                    '2': { from: '08:00:00', to: '17:00:00' },
                    '3': { from: '08:00:00', to: '17:00:00' },
                    '4': { from: '08:00:00', to: '17:00:00' },
                    '5': { from: '08:00:00', to: '17:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Рогато-Балковская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.878Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2cb',
        hash: '2019-10-11T14:03:51Z',
        nativeId: '40051',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40051',
                category: 'place',
                createDate: '2019-10-08T13:14:38Z',
                updateDate: '2019-10-11T14:03:51Z'
            },
            general: {
                id: 40051,
                name: 'Пристеньская библиотека',
                description:
                    '<p>Пристеньская библиотека является филиалом МБУК «Центральная библиотека Ровеньского района». </p><p>Пристеньская сельская библиотека была открыта в августе 1955 года. До этого в селе не было библиотеки, а работала изба-читальня с книжным фондом 200 экземпляров книг и журналов. Этой избой-читальней заведовал заведующий сельским клубом Кравцов Григорий Тимофеевич. Книги находились в клубе, в книжном шкафу. Все они были переданы в избу-читальню жителями села. </p><p>За время работы библиотеки она сменила несколько зданий. В 1996 году библиотеку переводят в здание средней школы в центре села. Ей выделяют часть коридора с отдельным входом. Общая площадь комнаты – 44 кв. м. В этом помещении библиотека располагается по настоящее время. </p><p>Библиотека сегодня: </p><ul><li>фонд – более 5 000 экз.;</li><li>число пользователей – 555;</li><li>документовыдача – более 12 000 ед.</li></ul><p>Библиотека является культурным и информационным центром на селе. Читателям предоставлен свободный доступ к книжному фонду, бесплатный доступ к сетевым ресурсам, к пользованию услугами МБА. Библиотека участвует в фестивале славянской культуры «Зеленые святки», в районном фестивале «От степной слободы начало». Яркими событиями стали: «Библионочь», День славянской письменности и культуры, престольный праздник с. Пристень.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Центральная,д 92',
                    comment: '',
                    fiasHouseId: 'a1b33b06-88dd-4c91-9fb7-374b1d491769',
                    fiasStreetId: '70dc9d2c-b671-40ea-a6b9-f2ed6292b185',
                    fiasSettlementId: '4db6f7e4-2fdc-4f22-81b1-6e3fd2d885ed',
                    fiasAreaId: 'd6ebfb80-ac58-4533-9678-6afaf9423cb1',
                    fiasRegionId: '639efe9d-3fc8-4438-8e70-ec4f2321f2a7',
                    fullAddress: 'обл Белгородская,р-н Ровеньский,с Пристень,ул Центральная,д 92',
                    mapPosition: { type: 'Point', coordinates: [38.859887123107896, 50.10357006262752] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [{ value: '79205950379', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://bel.cultreg.ru/places/1626/pristenskaya-biblioteka',
                        serviceName: 'Культурный регион'
                    },
                    {
                        url: 'https://www.culture.ru/institutes/40828/pristenskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/d97e6769fc971544166bd9020fed2321.jpeg',
                    title: 'Pristen.jpg'
                },
                localeIds: [27, 2, 1],
                locale: { name: 'Ровеньский район', timezone: 'Europe/Moscow', sysName: 'rovenskiy-rayon', id: 27 },
                organization: {
                    id: 246,
                    name: 'МБУК «Центральная библиотека Ровеньского района»',
                    inn: '3117004475',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 61',
                        comment: '',
                        fiasStreetId: '3bc2520f-f43c-4387-b266-6f689886b77a',
                        fiasSettlementId: '822f6b23-c6d2-4bac-9e13-671a1a25a2fc',
                        fiasAreaId: 'd6ebfb80-ac58-4533-9678-6afaf9423cb1',
                        fiasRegionId: '639efe9d-3fc8-4438-8e70-ec4f2321f2a7',
                        fullAddress: 'обл Белгородская,р-н Ровеньский,п Ровеньки,ул Ленина,д 61'
                    },
                    subordinationIds: [27, 2, 1],
                    subordination: {
                        name: 'Ровеньский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'rovenskiy-rayon',
                        id: 27
                    },
                    localeIds: [27, 2, 1],
                    locale: { name: 'Ровеньский район', timezone: 'Europe/Moscow', sysName: 'rovenskiy-rayon', id: 27 },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Библиотека Ровеньская',
                            networkId: '290601821',
                            isPersonal: true,
                            updateDate: '2015-07-17T07:19:54Z',
                            createDate: '2015-07-17T07:19:54Z',
                            accountId: 307,
                            postingGroupId: 1614
                        },
                        {
                            network: 'vk',
                            name: 'Ровеньская библиотека',
                            networkId: '86531430',
                            updateDate: '2015-03-03T06:37:20Z',
                            createDate: '2015-02-03T12:36:05Z',
                            accountId: 307,
                            postingGroupId: 379
                        },
                        {
                            network: 'twitter',
                            name: ' Ровеньская б-ка',
                            networkId: '3014269509',
                            isPersonal: true,
                            updateDate: '2015-03-03T06:37:20Z',
                            createDate: '2015-02-03T12:39:51Z',
                            accountId: 309,
                            postingGroupId: 381
                        },
                        {
                            network: 'ok',
                            name: 'МБУК "Центральная библиотека Ровеньского района"',
                            networkId: '52916979105918',
                            updateDate: '2015-03-03T06:37:20Z',
                            createDate: '2015-02-03T12:40:56Z',
                            accountId: 310,
                            postingGroupId: 380
                        },
                        {
                            network: 'ok',
                            name: 'Ровеньская библиотека',
                            networkId: '553578971006',
                            isPersonal: true,
                            updateDate: '2015-07-17T07:27:54Z',
                            createDate: '2015-07-17T07:27:54Z',
                            accountId: 310,
                            postingGroupId: 1615
                        },
                        {
                            network: 'fb',
                            name: 'Tsentralnaya Biblioteka  Rovenskogo Rayona',
                            networkId: '332077953665375',
                            isPersonal: true,
                            updateDate: '2015-03-03T06:37:20Z',
                            createDate: '2015-02-03T12:39:19Z',
                            accountId: 308,
                            postingGroupId: 382
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' },
                    { id: 596, name: 'Доступная среда', sysName: 'dostupnaya-sreda' }
                ],
                workingSchedule: {
                    '1': { from: '09:00:00', to: '18:00:00' },
                    '2': { from: '09:00:00', to: '18:00:00' },
                    '3': { from: '09:00:00', to: '18:00:00' },
                    '4': { from: '09:00:00', to: '18:00:00' },
                    '5': { from: '09:00:00', to: '20:00:00' },
                    '6': { from: '09:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Пристеньская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.883Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2cd',
        hash: '2019-10-09T14:07:27Z',
        nativeId: '40028',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40028',
                category: 'place',
                createDate: '2019-10-03T01:39:29Z',
                updateDate: '2019-10-09T14:07:27Z'
            },
            general: {
                id: 40028,
                name: 'Библиотека-филиал № 3 г. Улан-Удэ',
                description:
                    '<p>Библиотека была открыта в 1939 году и располагалась в пос. Площадка. В 1958 г. библиотека переехала в новое здание на ул. Столичную, 1, где находится и по сегодняшний день. В январе 2005 г. Центральная библиотека микрорайона Загорск вошла в состав Централизованной библиотечной системы г. Улан-Удэ.</p><p>Сегодня библиотека – информационный и культурно-досуговый центр для жителей микрорайона. Фонд насчитывает 20 607 экземпляров документов, для читателей выписываются журналы, газеты, создаются медиаресурсы.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Столичная,д 1',
                    comment: '',
                    fiasHouseId: '20d51a95-ee98-4af7-8124-4523e21ce89c',
                    fiasStreetId: 'd02113fc-fce9-44e3-8435-7967f45ea8c3',
                    fiasCityId: '9fdcc25f-a3d0-4f28-8b61-40648d099065',
                    fiasRegionId: 'a84ebed3-153d-4ba9-8532-8bdf879e1f5a',
                    fullAddress: 'респ Бурятия,г Улан-Удэ,ул Столичная,д 1',
                    mapPosition: { type: 'Point', coordinates: [107.72264778614044, 51.86649493724615] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: {
                    website: 'http://cbs-uu.ru',
                    email: 'filial3@cbs-ulan-ude.ru',
                    phones: [{ value: '73012252075', comment: '' }]
                },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40808/biblioteka-filial-3-g-ulan-ude',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/837e05d871988c6ab9ca8b798b4a2508.jpeg',
                    title: 'IMG_20181207_152601-1-1024x645.jpg'
                },
                localeIds: [589, 225, 1],
                locale: { name: 'Улан-Удэ', timezone: 'Asia/Irkutsk', sysName: 'ulan-ude', id: 589 },
                organization: {
                    id: 10872,
                    name: 'МАУ ЦБС г. Улан-Удэ',
                    inn: '0323102031',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 17',
                        comment: '',
                        fiasHouseId: '36529150-1608-4bc6-86ce-b2ba9d6e830e',
                        fiasStreetId: '2c3fed81-d657-44dd-8340-83c81649d69b',
                        fiasCityId: '9fdcc25f-a3d0-4f28-8b61-40648d099065',
                        fiasRegionId: 'a84ebed3-153d-4ba9-8532-8bdf879e1f5a',
                        fullAddress: 'Респ Бурятия,г Улан-Удэ,ул Ленина,д 17'
                    },
                    subordinationIds: [589, 225, 1],
                    subordination: { name: 'Улан-Удэ', timezone: 'Asia/Irkutsk', sysName: 'ulan-ude', id: 589 },
                    localeIds: [589, 225, 1],
                    locale: { name: 'Улан-Удэ', timezone: 'Asia/Irkutsk', sysName: 'ulan-ude', id: 589 },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Центральная-Городская-Библиотека Им-И-Калашникова',
                            networkId: '196010219',
                            isPersonal: true,
                            updateDate: '2019-09-19T01:55:45Z',
                            createDate: '2019-09-19T01:55:45Z',
                            accountId: 24061,
                            postingGroupId: 23944
                        }
                    ]
                },
                tags: [
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '0': { from: '10:00:00', to: '18:00:00' },
                    '1': { from: '10:00:00', to: '18:00:00' },
                    '2': { from: '10:00:00', to: '18:00:00' },
                    '3': { from: '10:00:00', to: '18:00:00' },
                    '4': { from: '10:00:00', to: '18:00:00' },
                    '5': { from: '10:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека-филиал № 3 г. Улан-Удэ',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.904Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2cf',
        hash: '2019-10-14T13:28:09Z',
        nativeId: '40024',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40024',
                category: 'place',
                createDate: '2019-10-02T11:10:55Z',
                updateDate: '2019-10-14T13:28:09Z'
            },
            general: {
                id: 40024,
                name: 'Библиотека № 23 ст. Елизаветинской',
                description:
                    '<p>В настоящее время библиотеку посещают около двух тысяч читателей, ежегодно им выдается свыше 36 000 изданий. Книжный фонд составляет более 19 000 экземпляров. Для читателей библиотекари организуют около 100 различных мероприятий в год, сотрудники активно занимаются формированием и сохранением книжного фонда библиотеки. </p><p>Библиотека прошла путь от избы-читальни до современного информационно-досугового центра Елизаветинской. Здесь проходят теплые встречи с кубанскими писателями и художниками, ветеранами и другими интересными людьми. Читатели приходят сюда не только за книгами, журналами и газетами, но и просто пообщаться.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Советская,д 32',
                    comment: '',
                    fiasHouseId: '5eb08c27-3bac-4dd8-a9f7-ec5296b18bc7',
                    fiasStreetId: 'e9937a3a-d2aa-4990-8594-80967da0ab30',
                    fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                    fiasSettlementId: '495ff179-5b91-44ef-8a25-4092217e17ea',
                    fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                    fullAddress: 'край Краснодарский,г Краснодар,ст-ца Елизаветинская,ул Советская,д 32',
                    mapPosition: { type: 'Point', coordinates: [38.78476917743682, 45.047579705982145] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { website: 'http://www.neklib.kubannet.ru', phones: [{ value: '78612293571', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40834/biblioteka-23-st-elizavetinskoi',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/3f861a49b63f1d65130fe05166df164e.jpeg',
                    title: '23 ф.jpg'
                },
                localeIds: [1449, 175, 1],
                locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                organization: {
                    id: 5207,
                    name:
                        'МУК муниципального образования город Краснодар «Централизованная библиотечная система города Краснодара»',
                    inn: '2308092456',
                    type: 'mincult',
                    address: {
                        street: 'ул. Красная, 87/89',
                        fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                        fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                        fullAddress: 'Краснодарский край,г Краснодар,ул. Красная, 87/89'
                    },
                    subordinationIds: [1449, 175, 1],
                    subordination: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                    localeIds: [1449, 175, 1],
                    locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }
                },
                workingSchedule: {
                    '0': { from: '11:00:00', to: '18:00:00' },
                    '1': { from: '11:00:00', to: '18:00:00' },
                    '2': { from: '11:00:00', to: '18:00:00' },
                    '3': { from: '11:00:00', to: '18:00:00' },
                    '5': { from: '11:00:00', to: '18:00:00' },
                    '6': { from: '11:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека № 23 ст. Елизаветинской',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.907Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d0',
        hash: '2019-10-09T12:05:41Z',
        nativeId: '40022',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40022',
                category: 'place',
                createDate: '2019-10-02T11:01:57Z',
                updateDate: '2019-10-09T12:05:41Z'
            },
            general: {
                id: 40022,
                name: 'Детская библиотека № 24 станицы Елизаветинской',
                description:
                    'Детская библиотека № 24 стала информационным, образовательным и досуговым центром для детей станицы и их родителей. В год учреждение обслуживает около 2 000 человек. В фонде библиотеки – более 16 000 изданий: книги, журналы, газеты, электронные продукты. В течение года сотрудниками проводится около 90 массовых мероприятий. Каждое лето при библиотеке работает летняя оздоровительная площадка «Ромашка», проводятся спортивно-игровые программы.',
                status: 'accepted',
                address: {
                    street: 'ул Советская,д 32/34',
                    comment: '',
                    fiasStreetId: 'e9937a3a-d2aa-4990-8594-80967da0ab30',
                    fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                    fiasSettlementId: '495ff179-5b91-44ef-8a25-4092217e17ea',
                    fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                    fullAddress: 'край Краснодарский,г Краснодар,ст-ца Елизаветинская,ул Советская,д 32/34',
                    mapPosition: { type: 'Point', coordinates: [38.784790635108955, 45.047591076176275] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [{ value: '78612293572', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40804/detskaya-biblioteka-24-stanicy-elizavetinskoi',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/61a26ad225ef73d557cb3fe06551b565.jpeg',
                    title: 'P1120820.JPG'
                },
                localeIds: [1449, 175, 1],
                locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                organization: {
                    id: 5207,
                    name:
                        'МУК муниципального образования город Краснодар «Централизованная библиотечная система города Краснодара»',
                    inn: '2308092456',
                    type: 'mincult',
                    address: {
                        street: 'ул. Красная, 87/89',
                        fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                        fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                        fullAddress: 'Краснодарский край,г Краснодар,ул. Красная, 87/89'
                    },
                    subordinationIds: [1449, 175, 1],
                    subordination: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                    localeIds: [1449, 175, 1],
                    locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }
                },
                workingSchedule: {
                    '1': { from: '11:00:00', to: '18:00:00' },
                    '2': { from: '11:00:00', to: '18:00:00' },
                    '3': { from: '11:00:00', to: '18:00:00' },
                    '4': { from: '11:00:00', to: '18:00:00' },
                    '5': { from: '11:00:00', to: '18:00:00' },
                    '6': { from: '11:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Детская библиотека № 24 станицы Елизаветинской',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.909Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d1',
        hash: '2019-10-14T11:57:08Z',
        nativeId: '40021',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40021',
                category: 'place',
                createDate: '2019-10-02T09:01:41Z',
                updateDate: '2019-10-14T11:57:08Z'
            },
            general: {
                id: 40021,
                name: 'Библиотека-клуб им. Т. Г. Шевченко',
                description:
                    '<p>Согласно записи в первой инвентарной книге, хранящейся в архиве, библиотека-клуб им. Т. Г. Шевченко была основана в апреле 1984 года в здании школы № 65 поселка Калинино. Библиотека-клуб им. Т. Г. Шевченко – современный культурно-досуговый центр, здесь проходят концерты, вечера отдыха, утренники и литературно-музыкальные вечера. Библиотека является просветительским центром 1 800 читателей. Учреждение обслуживает на дому инвалидов. Ко всем знаменательным датам сотрудники библиотеки подготавливают крупные мероприятия, встречи с участниками Великой Отечественной войны, концертные программы, оформляют книжные выставки. </p><p>В настоящее время книжный фонд библиотеки насчитывает свыше 18 000 экземпляров.</p>',
                status: 'accepted',
                address: {
                    street: 'д 20/5',
                    comment: '',
                    fiasHouseId: 'd0ecf51d-285c-4098-a725-055ea146a93e',
                    fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                    fiasSettlementId: '659b4bfa-6434-4a2c-9a18-55f91ecb3275',
                    fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                    fullAddress: 'край Краснодарский,г Краснодар,п Березовый,д 20/5',
                    mapPosition: { type: 'Point', coordinates: [38.99431675672531, 45.15128404046987] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: {
                    website: 'http://www.neklib.kubannet.ru',
                    email: 'cbsgk.lib.21@gmail.com',
                    phones: [{ value: '78612113299', comment: '' }]
                },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40830/biblioteka-klub-im-t-g-shevchenko',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/eed10c9b3a793fa318b03fce4db3026c.jpeg',
                    title: 'orig.jpg'
                },
                localeIds: [1449, 175, 1],
                locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                organization: {
                    id: 5207,
                    name:
                        'МУК муниципального образования город Краснодар «Централизованная библиотечная система города Краснодара»',
                    inn: '2308092456',
                    type: 'mincult',
                    address: {
                        street: 'ул. Красная, 87/89',
                        fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                        fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                        fullAddress: 'Краснодарский край,г Краснодар,ул. Красная, 87/89'
                    },
                    subordinationIds: [1449, 175, 1],
                    subordination: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                    localeIds: [1449, 175, 1],
                    locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }
                },
                tags: [{ id: 392, name: 'Книги', sysName: 'knigi' }],
                workingSchedule: {
                    '1': { from: '11:00:00', to: '18:00:00' },
                    '2': { from: '11:00:00', to: '18:00:00' },
                    '3': { from: '11:00:00', to: '18:00:00' },
                    '4': { from: '11:00:00', to: '18:00:00' },
                    '5': { from: '11:00:00', to: '18:00:00' },
                    '6': { from: '11:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека-клуб им. Т. Г. Шевченко',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.911Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d2',
        hash: '2019-10-09T11:19:18Z',
        nativeId: '40019',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40019',
                category: 'place',
                createDate: '2019-10-02T08:10:51Z',
                updateDate: '2019-10-09T11:19:18Z'
            },
            general: {
                id: 40019,
                name: 'Библиотека им. Н. А. Добролюбова',
                description:
                    '<p>Более 5 000 читателей записываются в библиотеку ежегодно. Среди них дети, молодежь и старшее поколение. </p><p>Одно из основных направлений работы библиотеки – краеведение. Для посетителей юного поколения организованы клубы по интересам: детский познавательно-досуговый клуб «Узнавайки», гражданско-патриотический «Гражданин», краеведческий «Истоки». </p><p>На сайте МУК ЦБС имеется виртуальный музей, где представлены этнографическая и мемориальная экспозиции библиотеки им. Н. А. Добролюбова. Для читателей библиотеки ежегодно проводится 149 массовых мероприятий, которые посещают 2,5 тыс. человек. </p>',
                status: 'accepted',
                address: {
                    street: 'ул Кирова,д 183/2',
                    comment: '',
                    fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                    fiasSettlementId: 'ff393514-1d6e-4162-b656-d7be3c917783',
                    fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                    fullAddress: 'край Краснодарский,г Краснодар,п отделения N4 совхоза Пашковский,ул Кирова,д 183/2',
                    mapPosition: { type: 'Point', coordinates: [39.09555673599243, 45.02202137975345] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: {
                    website: 'http://www.neklib.kubannet.ru',
                    email: 'cbsgk.lib.11@gmail.com',
                    phones: [{ value: '78612375521', comment: '' }]
                },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40797/biblioteka-im-n-a-dobrolyubova',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/aa8b599f88834e1ef9f0cd05412ff403.jpeg',
                    title: 'Ф № 11.jpg'
                },
                localeIds: [1449, 175, 1],
                locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                organization: {
                    id: 5207,
                    name:
                        'МУК муниципального образования город Краснодар «Централизованная библиотечная система города Краснодара»',
                    inn: '2308092456',
                    type: 'mincult',
                    address: {
                        street: 'ул. Красная, 87/89',
                        fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                        fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                        fullAddress: 'Краснодарский край,г Краснодар,ул. Красная, 87/89'
                    },
                    subordinationIds: [1449, 175, 1],
                    subordination: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                    localeIds: [1449, 175, 1],
                    locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }
                },
                workingSchedule: {
                    '0': { from: '11:00:00', to: '19:00:00' },
                    '1': { from: '11:00:00', to: '19:00:00' },
                    '2': { from: '11:00:00', to: '19:00:00' },
                    '3': { from: '11:00:00', to: '19:00:00' },
                    '5': { from: '11:00:00', to: '19:00:00' },
                    '6': { from: '11:00:00', to: '19:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека им. Н. А. Добролюбова',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.913Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d3',
        hash: '2019-10-09T09:22:40Z',
        nativeId: '40014',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40014',
                category: 'place',
                createDate: '2019-10-01T11:49:25Z',
                updateDate: '2019-10-09T09:22:40Z'
            },
            general: {
                id: 40014,
                name: 'Ачикулакская детская библиотека',
                description:
                    '<p><span>Ачикулакская</span> детская библиотека открылась в 1953 году. Сегодня её фонд составляет более 24 000 экземпляров книг. Ежегодно библиотека обслуживает 1 500 читателей, им выдаётся более 31 000 экземпляров книг и журналов. С 2005 года библиотека работает по целевой комплексной программе по краеведению «Край мой единственный <span>–</span>&nbsp;родина светлая». При библиотеке работают клубы по интересам: «Память» и «Сказка».</p>',
                status: 'accepted',
                address: {
                    street: 'ул Гвардейская,д 5',
                    comment: '',
                    fiasStreetId: '4ac28e24-9945-4eed-b404-1d41855dc301',
                    fiasSettlementId: 'd3bc88f0-060b-4f08-9e48-9a29fba7f8ed',
                    fiasAreaId: 'ec4208b4-09dc-4abf-940e-35f33d374c9b',
                    fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                    fullAddress: 'край Ставропольский,р-н Нефтекумский,с Ачикулак,ул Гвардейская,д 5',
                    mapPosition: { type: 'Point', coordinates: [44.829441644178594, 44.546661833785535] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [{ value: '79064406037', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40790/achikulakskaya-detskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    { url: 'https://all.culture.ru/uploads/8fce9d889655523692a979fd5905d65e.jpeg', title: 'акция.JPG' },
                    {
                        url: 'https://all.culture.ru/uploads/fc976ab302fe966d7bcc2997308732d6.jpeg',
                        title: 'акция2.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/78e91e07df0ccc835bb7943bed03640f.jpeg',
                        title: 'день журнала.JPG'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/f63e586eaf6cd9ea0a66a016eea8d8e2.jpeg',
                        title: 'экскурсия знакомство.JPG'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/020a2978de88a24e8a92148758a83271.jpeg',
                    title: 'ач. биб..JPG'
                },
                localeIds: [2281, 176, 1],
                locale: {
                    name: 'Нефтекумский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'neftekumskiy-rayon',
                    id: 2281
                },
                organization: {
                    id: 22560,
                    name:
                        'Муниципальное казенное учреждение культуры «Централизованная библиотечная система» Нефтекумского городского округа',
                    inn: '2614019871',
                    type: 'mincult',
                    address: {
                        street: 'площадь Ленина,3',
                        comment: '',
                        fiasCityId: '47a5b97a-5cc1-4fa4-893f-0041590dd823',
                        fiasAreaId: 'ec4208b4-09dc-4abf-940e-35f33d374c9b',
                        fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                        fullAddress: 'край Ставропольский,р-н Нефтекумский,г Нефтекумск,площадь Ленина,3'
                    },
                    subordinationIds: [2281, 176, 1],
                    subordination: {
                        name: 'Нефтекумский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'neftekumskiy-rayon',
                        id: 2281
                    },
                    localeIds: [2281, 176, 1],
                    locale: {
                        name: 'Нефтекумский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'neftekumskiy-rayon',
                        id: 2281
                    }
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '0': { from: '09:00:00', to: '17:00:00' },
                    '1': { from: '09:00:00', to: '17:00:00' },
                    '2': { from: '09:00:00', to: '17:00:00' },
                    '3': { from: '09:00:00', to: '17:00:00' },
                    '4': { from: '09:00:00', to: '17:00:00' },
                    '5': { from: '09:00:00', to: '17:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Ачикулакская детская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.914Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d4',
        hash: '2019-10-09T09:00:40Z',
        nativeId: '40013',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40013',
                category: 'place',
                createDate: '2019-10-01T06:55:21Z',
                updateDate: '2019-10-09T09:00:40Z'
            },
            general: {
                id: 40013,
                name: 'Коробовщинский сельский библиотечный филиал',
                description:
                    '<p>Начал работу в 1976 году. </p><p>Коробовщинская библиотека-филиал сегодня:</p><ul><li>книжный фонд – 6 377;</li><li>число пользователей – 196;</li><li>общее количество посещений за год – 1 093, книговыдача составляет 8513.</li></ul><p>Коробовщинский сельский библиотечный филиал – единственное культурно-информационное и досуговое учреждение в посёлке. </p><p>Деятельность библиотеки в настоящее время полифункциональна. Наиболее социально значимой задачей является сохранение культурной и исторической памяти, сбережение и продвижение духовного и литературного наследия русского народа. Заведующая библиотекой работает над созданием привлекательного образа библиотеки, стимулированием интереса к книге, чтению у подрастающего поколения, формированию их читательской культуры. Особенно оживляется массовая работа с детьми во время летних школьных каникул. Два года на базе библиотеки успешно работает клуб летнего отдыха «Весёлые ребята». В рамках клуба проводятся познавательные, развлекательные мероприятия: игровые программы, конкурсы, часы полезных советов. </p><p>Библиотекой успешно осваиваются новые технологии: пользователям предоставляется доступ к сети Интернет, электронному каталогу, порталу «Государственные услуги РФ». </p>',
                status: 'accepted',
                address: {
                    street: 'д 4',
                    comment: '',
                    fiasHouseId: 'a79b5ec7-05f7-40cc-bf5b-c6c91b97a26e',
                    fiasSettlementId: 'b39d903b-c770-4752-9827-6e2f32cdf7b7',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,п Коробовщинский,д 4',
                    mapPosition: { type: 'Point', coordinates: [39.26974500000001, 56.265932000000014] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'biblkor@yandex.ru', phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40791/korobovshinskii-selskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/dc6a6591f87b5e7e4667e104289da4ad.jpeg',
                    title: 'IMG-20190929-WA0005.jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '2': { from: '10:00:00', to: '13:30:00' },
                    '3': { from: '10:00:00', to: '13:30:00' },
                    '4': { from: '10:00:00', to: '13:30:00' },
                    '5': { from: '10:00:00', to: '13:30:00' },
                    '6': { from: '10:00:00', to: '13:30:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Коробовщинский сельский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.916Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d5',
        hash: '2019-10-11T11:50:33Z',
        nativeId: '40002',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40002',
                category: 'place',
                createDate: '2019-09-27T10:30:05Z',
                updateDate: '2019-10-11T11:50:33Z'
            },
            general: {
                id: 40002,
                name: 'Затеречная поселковая библиотека',
                description:
                    'Поселковая библиотека является информационным, культурным и просветительским центром для жителей посёлка. История библиотеки – часть общей истории посёлка нефтяников. Основана 4 августа 1957 г. Читателями библиотеки являются 1 500 жителей. Будучи открытой и общедоступной, поселковая библиотека оказывает услуги широкой аудитории. Фонд – более 25 000 экземпляров. Библиотека обслуживает читателей и жителей посёлка от 14 лет и старше.',
                status: 'accepted',
                address: {
                    street: 'ул Комсомольская,д 22',
                    comment: '',
                    fiasHouseId: 'fce867fa-880d-4480-9327-1ca48c496eb1',
                    fiasStreetId: 'dea47b13-222f-457e-bce8-0d48643642f5',
                    fiasSettlementId: 'e3ef00b8-aeb7-4fe7-b7c8-b113e222a2b7',
                    fiasAreaId: 'ec4208b4-09dc-4abf-940e-35f33d374c9b',
                    fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                    fullAddress: 'край Ставропольский,р-н Нефтекумский,п Затеречный,ул Комсомольская,д 22',
                    mapPosition: { type: 'Point', coordinates: [45.216947793960564, 44.79118963785516] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [{ value: '78655824843', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40820/zaterechnaya-poselkovaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/103b269857473d9c6bcc4e8a6c143c6f.jpeg',
                        title: 'IMG_20180420_161152.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/42cc6705b43db4e5d2713be88e31703b.jpeg',
                        title: 'IMG_20180320_144103.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/933aba78b20aa07ca9dd73a7c19e1552.jpeg',
                        title: 'IMG_20181030_114626.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/12b8cd41fa888f89af199f974f14bde9.jpeg',
                        title: 'IMG_20180510_131101.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/378d679c1f67a8c66f5f388bfb5e7739.jpeg',
                        title: 'IMG-20180330-WA0010.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/60fc65082ae3b6e4d3c512c87b15ae52.jpeg',
                        title: 'PIC_5373.JPG'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/0dc644b46a51dfea16b28da091ed9e50.jpeg',
                        title: 'PIC_1181.JPG'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/f410aba4ac37dba8e345fbb2ffcd17c1.jpeg',
                    title: 'IMG_20190920_110308.jpg'
                },
                localeIds: [2281, 176, 1],
                locale: {
                    name: 'Нефтекумский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'neftekumskiy-rayon',
                    id: 2281
                },
                organization: {
                    id: 22560,
                    name:
                        'Муниципальное казенное учреждение культуры «Централизованная библиотечная система» Нефтекумского городского округа',
                    inn: '2614019871',
                    type: 'mincult',
                    address: {
                        street: 'площадь Ленина,3',
                        comment: '',
                        fiasCityId: '47a5b97a-5cc1-4fa4-893f-0041590dd823',
                        fiasAreaId: 'ec4208b4-09dc-4abf-940e-35f33d374c9b',
                        fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                        fullAddress: 'край Ставропольский,р-н Нефтекумский,г Нефтекумск,площадь Ленина,3'
                    },
                    subordinationIds: [2281, 176, 1],
                    subordination: {
                        name: 'Нефтекумский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'neftekumskiy-rayon',
                        id: 2281
                    },
                    localeIds: [2281, 176, 1],
                    locale: {
                        name: 'Нефтекумский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'neftekumskiy-rayon',
                        id: 2281
                    }
                },
                workingSchedule: {
                    '0': { from: '08:00:00', to: '17:00:00' },
                    '1': { from: '08:00:00', to: '17:00:00' },
                    '2': { from: '08:00:00', to: '17:00:00' },
                    '3': { from: '08:00:00', to: '17:00:00' },
                    '4': { from: '08:00:00', to: '17:00:00' },
                    '5': { from: '08:00:00', to: '17:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Затеречная поселковая библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.918Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d6',
        hash: '2019-10-07T14:58:45Z',
        nativeId: '40001',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40001',
                category: 'place',
                createDate: '2019-09-27T10:18:00Z',
                updateDate: '2019-10-07T14:58:45Z'
            },
            general: {
                id: 40001,
                name: 'Флорищинский библиотечный филиал',
                description:
                    '<p>Был открыт в 1950 году. </p><p>Флорищинский сельский библиотечный филиал сегодня: </p><ul><li>книжный фонд – 7 117 единиц хранения; </li><li>число пользователей – 346; </li><li>общее количество посещений за год – 1 416; </li><li>количество книговыдач – 3 864. </li></ul>Флорищинский сельский библиотечный филиал – это единственное культурное и досуговое место на селе.<br /><p>Заведующая уделяет внимание сотрудничеству с местной администрацией, старается создать комфортные условия для пользователей, работает над привлекательным образом библиотеки, ведет справочно-информационное обслуживание пользователей.</p><p> Заведующая библиотекой организует массовые мероприятия для различных групп пользователей (дети, взрослые, пенсионеры), оформляет книжные выставки-просмотры литературы, старается внедрить компьютерные технологии в процесс библиотечной работы. </p>',
                status: 'accepted',
                address: {
                    street: 'ул Первая,д 6',
                    comment: '',
                    fiasHouseId: '1359d117-1e24-4043-a5c7-788553558b89',
                    fiasStreetId: '93c4688d-aaec-4d50-b4ec-3d1f33fc09d8',
                    fiasSettlementId: 'c08ee083-d816-4417-96c5-6bcb28352b06',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,с Флорищи,ул Первая,д 6',
                    mapPosition: { type: 'Point', coordinates: [39.22880000000001, 56.305324] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'florizibibl@gmail.com', phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40773/florishinskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/a46ea9ac02b24b56c9f8e4be596bb8ce.jpeg',
                    title: 'IMG-20190927-WA0011.jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '0': { from: '10:00:00', to: '14:00:00' },
                    '1': { from: '10:00:00', to: '14:00:00' },
                    '2': { from: '10:00:00', to: '14:00:00' },
                    '3': { from: '10:00:00', to: '14:00:00' },
                    '4': { from: '10:00:00', to: '14:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Флорищинский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.919Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d7',
        hash: '2019-10-07T14:56:34Z',
        nativeId: '40000',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/40000',
                category: 'place',
                createDate: '2019-09-27T10:15:57Z',
                updateDate: '2019-10-07T14:56:34Z'
            },
            general: {
                id: 40000,
                name: 'Ельцинский библиотечный филиал',
                description:
                    '<p>Открыт в 1936 году. Книжный фонд составляет 9 363 экземпляров, количество пользователей – 130, за год проводится 57 мероприятий. Библиотека – единственное культурное учреждение на селе – полифункциональный центр, в котором сочетаются традиционные формы с информационным пространством. Библиотека является центром досуга и общения для населения, стремится создать свою особую культурную, духовную информационную среду, опираясь в первую очередь на работу с книгой. </p><p>Являясь доступной для всех категорий граждан, библиотека отвечает различным запросам читателей. Заведующая библиотекой ведет исследовательскую работу по краеведению, изучению прошлого родного села. Здесь собраны тематические папки-накопители по истории храма села Ельцино и развитию сельскохозяйственных предприятий: «История села Ельцина», «Никто не забыт, ничто не забыто», «Наши знаменитые земляки», «Дорога к Храму». </p>',
                status: 'accepted',
                address: {
                    street: 'ул Школьная,д 13',
                    comment: '',
                    fiasHouseId: 'f208f5ff-6e3a-47ff-9d0f-aa0bd9e849f2',
                    fiasStreetId: 'c5772297-2a3c-42ea-a008-5e5637552a62',
                    fiasSettlementId: 'd201e99b-b414-42fe-bdbb-1f087bc3472d',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,с Ельцино,ул Школьная,д 13',
                    mapPosition: { type: 'Point', coordinates: [39.609739000000005, 56.207983999999996] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40774/elcinskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/86258e940bf402796f1a1336700f9f3e.jpeg',
                    title: 'IMG-20190927-WA0017.jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '0': { from: '10:00:00', to: '17:00:00' },
                    '1': { from: '10:00:00', to: '17:00:00' },
                    '2': { from: '10:00:00', to: '17:00:00' },
                    '3': { from: '10:00:00', to: '17:00:00' },
                    '4': { from: '10:00:00', to: '17:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Ельцинский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.922Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d8',
        hash: '2019-10-07T14:22:42Z',
        nativeId: '39992',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39992',
                category: 'place',
                createDate: '2019-09-26T13:32:35Z',
                updateDate: '2019-10-07T14:22:42Z'
            },
            general: {
                id: 39992,
                name: 'Библиотека им. К. А. Обойщикова',
                description:
                    'Фонд библиотеки – 20 тыс. экз. На сегодняшний день читателями библиотеки являются 3 500 человек. Большой популярностью пользуется художественная и научно-популярная литература. Ежегодно библиотека проводит около 100 мероприятий различной направленности. Деятельность учреждения отмечена дипломами, грамотами, благодарственными письмами, о нем пишут как о центре просвещения и культурного досуга для жителей теперь уже микрорайона им. маршала Г. К. Жукова и ближайших окрестностей.',
                status: 'accepted',
                address: {
                    street: 'ул им. Дзержинского,д 213',
                    comment: '',
                    fiasHouseId: 'ff3fa3a4-dc33-4bda-8500-9dcba4be92f7',
                    fiasStreetId: 'ffeb16d3-5647-4824-9b1a-f94aa1295c41',
                    fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                    fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                    fullAddress: 'край Краснодарский,г Краснодар,ул им. Дзержинского,д 213',
                    mapPosition: { type: 'Point', coordinates: [38.979625, 45.099619000000004] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { website: 'http://www.neklib.kubannet.ru', phones: [{ value: '78612585137', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40776/biblioteka-im-k-a-oboishikova',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/14530911ab15af62ec68cd503d3c65a2.jpeg',
                    title: 'XXL (2).jpg'
                },
                localeIds: [1449, 175, 1],
                locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                organization: {
                    id: 5207,
                    name:
                        'МУК муниципального образования город Краснодар «Централизованная библиотечная система города Краснодара»',
                    inn: '2308092456',
                    type: 'mincult',
                    address: {
                        street: 'ул. Красная, 87/89',
                        fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                        fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                        fullAddress: 'Краснодарский край,г Краснодар,ул. Красная, 87/89'
                    },
                    subordinationIds: [1449, 175, 1],
                    subordination: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                    localeIds: [1449, 175, 1],
                    locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }
                },
                workingSchedule: {
                    '0': { from: '11:00:00', to: '19:00:00' },
                    '1': { from: '11:00:00', to: '19:00:00' },
                    '2': { from: '11:00:00', to: '19:00:00' },
                    '3': { from: '11:00:00', to: '19:00:00' },
                    '5': { from: '11:00:00', to: '19:00:00' },
                    '6': { from: '11:00:00', to: '19:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека им. К. А. Обойщикова',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.926Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2d9',
        hash: '2019-10-07T14:20:27Z',
        nativeId: '39991',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39991',
                category: 'place',
                createDate: '2019-09-26T13:04:44Z',
                updateDate: '2019-10-07T14:20:27Z'
            },
            general: {
                id: 39991,
                name: 'Библиотека им. В. В. Маяковского',
                description:
                    '<p>Фонд библиотеки составляет около 40 тыс. экземпляров документов. Услугами филиала пользуется больше 7 тыс. читателей. Библиотека располагает 12-томным собранием сочинений В. В. Маяковского, десятком сборников произведений поэта.</p><p>С 1992 года библиотека работает как профилированный филиал православной культуры. Располагает она и специализированным фондом – исторической и духовной литературы.</p><p>Для юношей и девушек в библиотеке организован клуб по интересам «Мое открытие России». Библиотека стремится всегда быть центром просвещения, духовно-нравственного и патриотического воспитания, местом культурного досуга жителей Юбилейного микрорайона.</p>',
                status: 'accepted',
                address: {
                    street: 'ул им. 70-летия Октября,д 26',
                    comment: '',
                    fiasHouseId: 'fe4d0295-8e79-4558-96be-71c47dc76680',
                    fiasStreetId: '2705f9e9-6e5a-4fa4-b438-195cf60961ba',
                    fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                    fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                    fullAddress: 'край Краснодарский,г Краснодар,ул им. 70-летия Октября,д 26',
                    mapPosition: { type: 'Point', coordinates: [38.906359076499946, 45.033876979808646] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { website: 'http://www.neklib.kubannet.ru', phones: [{ value: '78612613115', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40777/biblioteka-im-v-v-mayakovskogo',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: { url: 'https://all.culture.ru/uploads/86c8cbe6ebbfe3e6db809ab7ef7387d0.jpg', title: 'L.jpg' },
                localeIds: [1449, 175, 1],
                locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                organization: {
                    id: 5207,
                    name:
                        'МУК муниципального образования город Краснодар «Централизованная библиотечная система города Краснодара»',
                    inn: '2308092456',
                    type: 'mincult',
                    address: {
                        street: 'ул. Красная, 87/89',
                        fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                        fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                        fullAddress: 'Краснодарский край,г Краснодар,ул. Красная, 87/89'
                    },
                    subordinationIds: [1449, 175, 1],
                    subordination: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                    localeIds: [1449, 175, 1],
                    locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }
                },
                recommendations: [{ name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }],
                tags: [{ id: 28, name: 'Литература', sysName: 'literatura' }],
                workingSchedule: {
                    '0': { from: '11:00:00', to: '19:00:00' },
                    '1': { from: '11:00:00', to: '19:00:00' },
                    '2': { from: '11:00:00', to: '19:00:00' },
                    '3': { from: '11:00:00', to: '19:00:00' },
                    '5': { from: '11:00:00', to: '19:00:00' },
                    '6': { from: '11:00:00', to: '19:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека им. В. В. Маяковского',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.928Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2da',
        hash: '2019-10-07T14:17:48Z',
        nativeId: '39990',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39990',
                category: 'place',
                createDate: '2019-09-26T13:00:51Z',
                updateDate: '2019-10-07T14:17:48Z'
            },
            general: {
                id: 39990,
                name: 'Библиотека им. Л. Н. Толстого',
                description:
                    '<p>В настоящее время в библиотеке им. Л. Н. Толстого читают около 5 тыс. краснодарцев. Ежегодно библиотеку посещают свыше 40 тыс. раз, читателям выдается свыше 98 тыс. печатных и электронных изданий. Для читателей библиотекари организуют книжные тематические выставки, проводят премьеры книг, литературные вечера и праздники, встречи с писателями и поэтами, обзоры литературы. </p><p>Сегодня библиотека – это информационный и просветительский центр микрорайона в области делового чтения. Также особый акцент делается на работе с молодежью в сфере воспитания гражданственности, нравственности, культуры. При библиотеке работают клуб по интересам «Кругозор» для старшего поколения, видеоклуб для молодежи «Листая страницы, пересматривая фильм», кружок декоративно-прикладного творчества «Волшебные узоры». </p>',
                status: 'accepted',
                address: {
                    street: 'ул им. Тургенева,д 140/2',
                    comment: '',
                    fiasHouseId: '3dcab480-f1e9-47de-852c-ce7977e64567',
                    fiasStreetId: 'ff730a7d-cd80-47df-b962-40f8b1f498c8',
                    fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                    fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                    fullAddress: 'край Краснодарский,г Краснодар,ул им. Тургенева,д 140/2',
                    mapPosition: { type: 'Point', coordinates: [38.962692000000004, 45.064386999999996] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { website: 'http://www.neklib.kubannet.ru', phones: [{ value: '78612592928', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40778/biblioteka-im-l-n-tolstogo',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/6921ee7cddcadc31ec01454b5abd8646.jpeg',
                    title: 'толстого.jpg'
                },
                localeIds: [1449, 175, 1],
                locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                organization: {
                    id: 5207,
                    name:
                        'МУК муниципального образования город Краснодар «Централизованная библиотечная система города Краснодара»',
                    inn: '2308092456',
                    type: 'mincult',
                    address: {
                        street: 'ул. Красная, 87/89',
                        fiasCityId: '7dfa745e-aa19-4688-b121-b655c11e482f',
                        fiasRegionId: 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8',
                        fullAddress: 'Краснодарский край,г Краснодар,ул. Красная, 87/89'
                    },
                    subordinationIds: [1449, 175, 1],
                    subordination: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 },
                    localeIds: [1449, 175, 1],
                    locale: { name: 'Краснодар', timezone: 'Europe/Moscow', sysName: 'krasnodar', id: 1449 }
                },
                tags: [
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' }
                ],
                workingSchedule: {
                    '1': { from: '11:00:00', to: '18:00:00' },
                    '2': { from: '11:00:00', to: '18:00:00' },
                    '3': { from: '11:00:00', to: '18:00:00' },
                    '4': { from: '11:00:00', to: '18:00:00' },
                    '5': { from: '11:00:00', to: '18:00:00' },
                    '6': { from: '11:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека им. Л. Н. Толстого',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.935Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2db',
        hash: '2019-10-07T14:51:26Z',
        nativeId: '39989',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39989',
                category: 'place',
                createDate: '2019-09-26T11:40:33Z',
                updateDate: '2019-10-07T14:51:26Z'
            },
            general: {
                id: 39989,
                name: 'Раздольевский библиотечный филиал',
                description:
                    '<p>Открыт в 1965 году. На сегодняшний день книжный фонд составляет 7 379 единиц хранения. Количество читателей за год – 541. Количество книговыдач – 14 479. Количество посещений – 7 571. Библиотека – информационный и культурный центр для жителей посёлка, подключена к сети Интернет. </p><p>Практика работы направлена на создание привлекательного имиджа библиотеки в местном сообществе, стимулированию интереса к чтению детей, подростков, формированию их читательской культуры. Библиотека активно сотрудничает с местным Домом культуры, местной администрацией, Стенковской основной школой, детским дошкольным учреждением. При библиотеке созданы и успешно работают женский клуб «Хозяюшка», детский кружок «Акварель». </p><p> Библиотека предоставляет следующие информационные услуги: </p><ul><li>доступ к электронному каталогу, виртуальной справке, порталу «Госуслуги»; </li><li>онлайн продление литературы; </li><li>электронная доставка документов.</li></ul>',
                status: 'accepted',
                address: {
                    street: 'ул Совхозная,д 6',
                    comment: '',
                    fiasHouseId: '6d731352-831c-4133-8b16-cab3bfb77953',
                    fiasStreetId: '30639341-c368-4d90-8e05-29d6f78429cc',
                    fiasSettlementId: 'dd2063f2-2946-4fbc-9565-168d18f23aeb',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,п Раздолье,ул Совхозная,д 6',
                    mapPosition: { type: 'Point', coordinates: [39.45942399999999, 56.274979] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'biblrazd@mail.ru', phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40779/razdolevskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/f1208ac4e9465b047084822c6578a447.jpeg',
                    title: 'IMG-360264c9aa1b5985df695c327a0437c3-V (1).jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '10:00:00', to: '18:00:00' },
                    '2': { from: '10:00:00', to: '18:00:00' },
                    '3': { from: '10:00:00', to: '18:00:00' },
                    '4': { from: '10:00:00', to: '18:00:00' },
                    '5': { from: '10:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Раздольевский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.937Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2dc',
        hash: '2019-10-07T13:52:40Z',
        nativeId: '39984',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39984',
                category: 'place',
                createDate: '2019-09-26T07:59:56Z',
                updateDate: '2019-10-07T13:52:40Z'
            },
            general: {
                id: 39984,
                name: 'Завалинский сельский библиотечный филиал',
                description:
                    '<p>Год образования – 1939, с 1970 года находится в помещении Дома культуры. Книжный фонд составляет 8 967 экземпляров, число пользователей в год – 300, за год выдаётся 7 200 экземпляров литературы. Завалинский сельский филиал был и остаётся неотъемлемой частью социальной структуры села, его общественной жизни, способствует сохранению историко-культурного наследия земли завалинской. </p><p>Заведующая филиалом использует современные информационные технологии, обеспечивающие доступ к ресурсам более крупных библиотек, пробуждает интерес населения к новым возможностям по использованию большого объёма знаний и информации, сосредоточенной в библиотеках района, области, России. В библиотеке предоставляется доступ пользователям к сети Интернет, электронному каталогу, порталу «Государственные услуги РФ», осуществляет онлайн-продление литературы и электронную доставку документов. </p><p>Заведующая библиотекой старается активно продвигать информацию о деятельности Завалинского филиала через блог «Кольчугинские библиотеки день за днём» на сайте МБУК «МЦБ». Большое внимание уделяется краеведческой работе. В библиотеке создана и пополняется <a href="http://libkolch.ru/malaya-rodina/proekt-istoriya-moego-sela/istoriya-sela-zavalino/" target="_blank">летопись села Завалино</a>, а также материалы о земляке Н. И. Крузенштерне. </p>',
                status: 'accepted',
                address: {
                    street: 'ул Первая,д 13',
                    comment: '',
                    fiasHouseId: 'bddbd493-7010-4666-b991-8de7635c44dd',
                    fiasStreetId: '6c2a48a3-c1bc-4569-98ee-b57605c134bf',
                    fiasSettlementId: '62300a93-d548-4ce6-b96c-37622456522a',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,п Вишневый,ул Первая,д 13',
                    mapPosition: { type: 'Point', coordinates: [40.898894, 55.904195] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40770/zavalinskii-selskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/a1fff0cfe5ceabdf7882ab008f29e572.jpeg',
                        title: 'IMG-6b13ea27e7b387b3b8fc65f0072c44dc-V (1).jpg'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/64b16a0c86f7f646f32d49ebdf6b27f7.jpeg',
                    title: 'IMG-8401cfcea79ee2bd9d8076b07619070e-V.jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' },
                    { id: 406, name: 'Краеведение', sysName: 'kraevedenie' }
                ],
                workingSchedule: {
                    '2': { from: '10:00:00', to: '18:00:00' },
                    '3': { from: '10:00:00', to: '18:00:00' },
                    '4': { from: '10:00:00', to: '18:00:00' },
                    '5': { from: '10:00:00', to: '18:00:00' },
                    '6': { from: '10:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Завалинский сельский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.939Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2dd',
        hash: '2019-10-01T12:54:22Z',
        nativeId: '39980',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39980',
                category: 'place',
                createDate: '2019-09-25T06:16:38Z',
                updateDate: '2019-10-01T12:54:22Z'
            },
            general: {
                id: 39980,
                name: 'Верх-Леманская библиотека',
                description:
                    '<p>Верх-Леманская библиотека была основана в 1955 году. Библиотека находилась в одном здании с сельским клубом. За два года работы библиотеки книжный фонд пополнился до 2 160 экземпляров. В 1974 году перед новым годом библиотека и клуб переехали в новое здание.</p><p> В настоящее время количество пользователей библиотеки – 175 человек. Книжный фонд составляет 7 071 экземпляр. При библиотеке работают клуб «Хозяюшка» (посещают женщины среднего возраста) и клуб «Ветеран» (для пенсионеров). Для детей работает кружок «Теремок».</p>',
                status: 'accepted',
                address: {
                    street: 'ул Школьная,д 15',
                    comment: '',
                    fiasHouseId: 'b2a433b4-1305-4361-9a8d-1b398d0f8bd2',
                    fiasStreetId: '74360ab5-8471-4408-a421-c9dabf563b6e',
                    fiasSettlementId: '57fdc807-440e-4cd3-8a4f-4f53f1a45e5d',
                    fiasAreaId: '9e825622-fe80-44af-a148-fc09ef8fdf0d',
                    fiasRegionId: '0b940b96-103f-4248-850c-26b6c7296728',
                    fullAddress: 'обл Кировская,р-н Афанасьевский,д Слобода,ул Школьная,д 15',
                    mapPosition: { type: 'Point', coordinates: [53.675506000000006, 58.674599] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [{ value: '78333124113', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40761/verkh-lemanskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/93e93d7c003c4942d2e9add2bbafbf0c.jpeg',
                    title: 'IMG_1041.JPG'
                },
                localeIds: [92, 85, 1],
                locale: {
                    name: 'Афанасьевский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'afanasevskiy-rayon',
                    id: 92
                },
                organization: {
                    id: 9847,
                    name: 'МБУК «Афанасьевская центральная районная библиотека»',
                    inn: '4302002072',
                    type: 'mincult',
                    address: {
                        street: 'ул Первомайская,д 11',
                        comment: '',
                        fiasStreetId: 'ec385de6-60a4-4c4a-8861-c5ac2a6fda23',
                        fiasSettlementId: 'aef94619-59f9-421b-a9e3-1949509a3c9c',
                        fiasAreaId: '9e825622-fe80-44af-a148-fc09ef8fdf0d',
                        fiasRegionId: '0b940b96-103f-4248-850c-26b6c7296728',
                        fullAddress: 'обл Кировская,р-н Афанасьевский,пгт Афанасьево,ул Первомайская,д 11'
                    },
                    subordinationIds: [92, 85, 1],
                    subordination: {
                        name: 'Афанасьевский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'afanasevskiy-rayon',
                        id: 92
                    },
                    localeIds: [92, 85, 1],
                    locale: {
                        name: 'Афанасьевский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'afanasevskiy-rayon',
                        id: 92
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Афанасьевская центральная районная библиотека',
                            networkId: '137357493',
                            updateDate: '2017-04-10T09:53:36Z',
                            createDate: '2017-04-10T09:53:36Z',
                            accountId: 13954,
                            postingGroupId: 12002
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '12:30:00', to: '16:00:00' },
                    '2': { from: '12:30:00', to: '16:00:00' },
                    '3': { from: '12:30:00', to: '16:00:00' },
                    '4': { from: '12:30:00', to: '16:00:00' },
                    '5': { from: '12:30:00', to: '16:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Верх-Леманская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.940Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2de',
        hash: '2019-09-27T08:19:24Z',
        nativeId: '39978',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39978',
                category: 'place',
                createDate: '2019-09-24T12:18:05Z',
                updateDate: '2019-09-27T08:19:24Z'
            },
            general: {
                id: 39978,
                name: 'Анкудиновская сельская библиотека',
                description:
                    '<p>В 1926 году в Анкудинове была открыта изба-читальня. Находилась она в бывшем доме священника. Изба-читальня была постоянным местом сбора молодежи близлежащих деревень. Работал здесь драмкружок, ставились спектакли. </p><p>Библиотека находилась в д. Дровново и в 1964 году была перевезена в помещение клуба д. Анкудиново. Туда же был перевезен и фонд избы-читальни. Общий книжный фонд Анкудиновской библиотеки составил 8 тыс. книг, посещали библиотеку свыше 500 человек. Работало 8 передвижных пунктов. </p><p>В 1976 году библиотека была переведена в другое, более просторное помещение.</p><p>В 2014 году (Постановление администрации Петушинского района от 31.12.2014 г. № 2639) переведена в здание бывшей школы. При библиотеке работает кружок «Умелые ручки». Книжный фонд составляет 7 075 экземпляров книг и журналов. Число читателей составляет 145 человек.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Арханинская,д 48',
                    comment: '',
                    fiasHouseId: 'ce79949d-a031-468c-8e82-6cb7921fa1c4',
                    fiasStreetId: '75f28279-8831-4c90-a422-932012b7a6ae',
                    fiasSettlementId: '1909777f-dcfb-497b-b9e3-ab80dc4562b1',
                    fiasAreaId: '4265d25d-8f14-4f0a-9c1f-9ad765c6c918',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Петушинский,д Анкудиново,ул Арханинская,д 48',
                    mapPosition: { type: 'Point', coordinates: [39.581532, 56.084776999999995] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40754/ankudinovskaya-selskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/c7748cb0dd1bd84b8586ff8cb1bb29c6.jpeg',
                        title: 'IMG_0154.JPG'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/4fd4431112a5cd834960035283476b19.jpeg',
                        title: 'IMG_0161.JPG'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/3e908278c136014e898d65e642054d66.jpeg',
                    title: 'IMG_0163.JPG'
                },
                localeIds: [536, 180, 1],
                locale: {
                    name: 'Петушинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'petushinskiy-rayon',
                    id: 536
                },
                organization: {
                    id: 18666,
                    name: 'МБУК «Межпоселенческая централизованная библиотечная система Петушинского района»',
                    inn: '3321027063',
                    type: 'mincult',
                    address: {
                        street: 'ул Строителей,д 12',
                        comment: '',
                        fiasHouseId: '153dd5cd-ea7a-43c9-b6f2-02e927b568ce',
                        fiasStreetId: 'dcba352a-94e0-439c-91ff-392f9338f633',
                        fiasCityId: 'b7ba6bfc-4b0c-4b05-88bd-267d50b36850',
                        fiasAreaId: '4265d25d-8f14-4f0a-9c1f-9ad765c6c918',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Петушинский,г Петушки,ул Строителей,д 12'
                    },
                    subordinationIds: [536, 180, 1],
                    subordination: {
                        name: 'Петушинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petushinskiy-rayon',
                        id: 536
                    },
                    localeIds: [536, 180, 1],
                    locale: {
                        name: 'Петушинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petushinskiy-rayon',
                        id: 536
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Аннинская Сельская-Библиотека',
                            networkId: '556479238',
                            isPersonal: true,
                            updateDate: '2019-10-10T07:42:45Z',
                            createDate: '2019-10-10T07:42:45Z',
                            accountId: 27018,
                            postingGroupId: 24153
                        },
                        {
                            network: 'vk',
                            name: 'Караваевская сельская библиотека МБУК "МЦБС Пету',
                            networkId: '147183014',
                            updateDate: '2019-09-22T11:04:50Z',
                            createDate: '2019-09-22T11:04:50Z',
                            accountId: 26853,
                            postingGroupId: 23975
                        },
                        {
                            network: 'vk',
                            name: 'Костинская сельская библиотека',
                            networkId: '186443201',
                            updateDate: '2019-10-11T09:18:50Z',
                            createDate: '2019-10-11T09:18:50Z',
                            accountId: 27065,
                            postingGroupId: 24208
                        },
                        {
                            network: 'vk',
                            name: 'МБУК "МЦБС Петушинского района"',
                            networkId: '117833536',
                            updateDate: '2017-07-04T12:08:49Z',
                            createDate: '2017-07-04T12:08:49Z',
                            accountId: 15692,
                            postingGroupId: 13581
                        },
                        {
                            network: 'vk',
                            name: 'Нагорная сельская библиотека',
                            networkId: '131516604',
                            updateDate: '2019-10-04T07:38:03Z',
                            createDate: '2019-10-04T07:38:03Z',
                            accountId: 26949,
                            postingGroupId: 24072
                        },
                        {
                            network: 'vk',
                            name: 'Пахомовская сельская библиотека',
                            networkId: '166589326',
                            updateDate: '2019-10-04T08:33:47Z',
                            createDate: '2019-10-04T08:33:47Z',
                            accountId: 26954,
                            postingGroupId: 24076
                        },
                        {
                            network: 'vk',
                            name: 'Светлана Длэц',
                            networkId: '328390229',
                            isPersonal: true,
                            updateDate: '2019-10-04T08:52:16Z',
                            createDate: '2019-10-04T08:52:16Z',
                            accountId: 26955,
                            postingGroupId: 24077
                        },
                        {
                            network: 'vk',
                            name: 'Читай, Петушинский район',
                            networkId: '86380164',
                            updateDate: '2019-09-03T13:03:40Z',
                            createDate: '2019-08-29T08:06:41Z',
                            accountId: 15692,
                            postingGroupId: 23716
                        },
                        {
                            network: 'twitter',
                            name: 'МБУК «МЦБС Петушинского района»',
                            networkId: '1166647943866310661',
                            isPersonal: true,
                            updateDate: '2019-08-29T13:58:22Z',
                            createDate: '2019-08-29T13:58:22Z',
                            accountId: 26609,
                            postingGroupId: 23729
                        },
                        {
                            network: 'twitter',
                            name: 'Читай, Петушинский район',
                            networkId: '1168896224608567296',
                            isPersonal: true,
                            updateDate: '2019-09-03T14:47:14Z',
                            createDate: '2019-09-03T14:47:14Z',
                            accountId: 26652,
                            postingGroupId: 23770
                        },
                        {
                            network: 'ok',
                            name: 'Аннинская сельская библиотека',
                            networkId: '578852312253',
                            isPersonal: true,
                            updateDate: '2019-10-11T10:06:48Z',
                            createDate: '2019-10-11T10:06:48Z',
                            accountId: 27070,
                            postingGroupId: 24213
                        },
                        {
                            network: 'ok',
                            name: 'Аннинская сельская библиотека',
                            networkId: '56117699215549',
                            updateDate: '2019-10-11T10:06:45Z',
                            createDate: '2019-10-11T10:06:45Z',
                            accountId: 27070,
                            postingGroupId: 24212
                        },
                        {
                            network: 'ok',
                            name: 'МБУК "МЦБС Петушинского района"',
                            networkId: '52364109611148',
                            updateDate: '2019-08-27T08:34:36Z',
                            createDate: '2017-07-04T12:05:15Z',
                            accountId: 15687,
                            postingGroupId: 13580
                        },
                        {
                            network: 'ok',
                            name: 'Пахомовская библиотека',
                            networkId: '61145869451299',
                            updateDate: '2019-10-04T08:06:00Z',
                            createDate: '2019-10-04T08:06:00Z',
                            accountId: 26953,
                            postingGroupId: 24075
                        },
                        {
                            network: 'ok',
                            name: 'غغغغغ Ęlena Prokhorovaۼۼۼۼ',
                            networkId: '442617108876',
                            isPersonal: true,
                            updateDate: '2019-10-02T19:32:12Z',
                            createDate: '2019-10-02T19:32:12Z',
                            accountId: 15687,
                            postingGroupId: 24060
                        }
                    ]
                },
                recommendations: [
                    { name: 'Петушинский район', timezone: 'Europe/Moscow', sysName: 'petushinskiy-rayon', id: 536 }
                ],
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '10:00:00', to: '18:00:00' },
                    '2': { from: '10:00:00', to: '18:00:00' },
                    '3': { from: '10:00:00', to: '18:00:00' },
                    '4': { from: '10:00:00', to: '18:00:00' },
                    '5': { from: '10:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Анкудиновская сельская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.942Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2df',
        hash: '2019-09-27T08:17:15Z',
        nativeId: '39976',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39976',
                category: 'place',
                createDate: '2019-09-24T12:16:17Z',
                updateDate: '2019-09-27T08:17:15Z'
            },
            general: {
                id: 39976,
                name: 'Пекшинская сельская библиотека',
                description:
                    '<p>19 января 1966 года профкомом совхоза «Петушинский» была открыта Пекшинская сельская библиотека. Находилась она в квартире жилого дома. </p><p>До открытия библиотеки с 1960 по 1966 годы работал пункт выдачи книги, который находился в механической мастерской совхоза и обслуживался Болдинской сельской библиотекой. </p><p>В ноябре 1967 года библиотека переехала в открывшийся сельский Дом культуры. </p><p>В 1992 году библиотека профкома вошла в состав Петушинской ЦБС. </p><p>При библиотеке работают кружки для взрослых: «Литературная гостиная» и «Деревенские посиделки».</p><p>Книжный фонд составляет 9 850 экземпляров книг и журналов. Число читателей составляет 400 человек.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Центральная,д 6',
                    comment: '',
                    fiasHouseId: 'f0c8f4b6-bfb8-4445-8b87-91a0eb99087a',
                    fiasStreetId: '85650825-d049-48d7-a3e4-843285dc2ada',
                    fiasSettlementId: '3e3f460d-01f7-43c4-abfe-4999bfeb789a',
                    fiasAreaId: '4265d25d-8f14-4f0a-9c1f-9ad765c6c918',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Петушинский,д Пекша,ул Центральная,д 6',
                    mapPosition: { type: 'Point', coordinates: [39.69637155532837, 55.964989985927104] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [{ value: '74924357289', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40755/pekshinskaya-selskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/b58e85ee8d9970ab6fa2af406f47e8e5.jpeg',
                        title: 'IMG_0554.JPG'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/206342816dd8db255f8fa612beadcd46.jpeg',
                    title: 'IMG_0557.JPG'
                },
                localeIds: [536, 180, 1],
                locale: {
                    name: 'Петушинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'petushinskiy-rayon',
                    id: 536
                },
                organization: {
                    id: 18666,
                    name: 'МБУК «Межпоселенческая централизованная библиотечная система Петушинского района»',
                    inn: '3321027063',
                    type: 'mincult',
                    address: {
                        street: 'ул Строителей,д 12',
                        comment: '',
                        fiasHouseId: '153dd5cd-ea7a-43c9-b6f2-02e927b568ce',
                        fiasStreetId: 'dcba352a-94e0-439c-91ff-392f9338f633',
                        fiasCityId: 'b7ba6bfc-4b0c-4b05-88bd-267d50b36850',
                        fiasAreaId: '4265d25d-8f14-4f0a-9c1f-9ad765c6c918',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Петушинский,г Петушки,ул Строителей,д 12'
                    },
                    subordinationIds: [536, 180, 1],
                    subordination: {
                        name: 'Петушинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petushinskiy-rayon',
                        id: 536
                    },
                    localeIds: [536, 180, 1],
                    locale: {
                        name: 'Петушинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petushinskiy-rayon',
                        id: 536
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Аннинская Сельская-Библиотека',
                            networkId: '556479238',
                            isPersonal: true,
                            updateDate: '2019-10-10T07:42:45Z',
                            createDate: '2019-10-10T07:42:45Z',
                            accountId: 27018,
                            postingGroupId: 24153
                        },
                        {
                            network: 'vk',
                            name: 'Караваевская сельская библиотека МБУК "МЦБС Пету',
                            networkId: '147183014',
                            updateDate: '2019-09-22T11:04:50Z',
                            createDate: '2019-09-22T11:04:50Z',
                            accountId: 26853,
                            postingGroupId: 23975
                        },
                        {
                            network: 'vk',
                            name: 'Костинская сельская библиотека',
                            networkId: '186443201',
                            updateDate: '2019-10-11T09:18:50Z',
                            createDate: '2019-10-11T09:18:50Z',
                            accountId: 27065,
                            postingGroupId: 24208
                        },
                        {
                            network: 'vk',
                            name: 'МБУК "МЦБС Петушинского района"',
                            networkId: '117833536',
                            updateDate: '2017-07-04T12:08:49Z',
                            createDate: '2017-07-04T12:08:49Z',
                            accountId: 15692,
                            postingGroupId: 13581
                        },
                        {
                            network: 'vk',
                            name: 'Нагорная сельская библиотека',
                            networkId: '131516604',
                            updateDate: '2019-10-04T07:38:03Z',
                            createDate: '2019-10-04T07:38:03Z',
                            accountId: 26949,
                            postingGroupId: 24072
                        },
                        {
                            network: 'vk',
                            name: 'Пахомовская сельская библиотека',
                            networkId: '166589326',
                            updateDate: '2019-10-04T08:33:47Z',
                            createDate: '2019-10-04T08:33:47Z',
                            accountId: 26954,
                            postingGroupId: 24076
                        },
                        {
                            network: 'vk',
                            name: 'Светлана Длэц',
                            networkId: '328390229',
                            isPersonal: true,
                            updateDate: '2019-10-04T08:52:16Z',
                            createDate: '2019-10-04T08:52:16Z',
                            accountId: 26955,
                            postingGroupId: 24077
                        },
                        {
                            network: 'vk',
                            name: 'Читай, Петушинский район',
                            networkId: '86380164',
                            updateDate: '2019-09-03T13:03:40Z',
                            createDate: '2019-08-29T08:06:41Z',
                            accountId: 15692,
                            postingGroupId: 23716
                        },
                        {
                            network: 'twitter',
                            name: 'МБУК «МЦБС Петушинского района»',
                            networkId: '1166647943866310661',
                            isPersonal: true,
                            updateDate: '2019-08-29T13:58:22Z',
                            createDate: '2019-08-29T13:58:22Z',
                            accountId: 26609,
                            postingGroupId: 23729
                        },
                        {
                            network: 'twitter',
                            name: 'Читай, Петушинский район',
                            networkId: '1168896224608567296',
                            isPersonal: true,
                            updateDate: '2019-09-03T14:47:14Z',
                            createDate: '2019-09-03T14:47:14Z',
                            accountId: 26652,
                            postingGroupId: 23770
                        },
                        {
                            network: 'ok',
                            name: 'Аннинская сельская библиотека',
                            networkId: '578852312253',
                            isPersonal: true,
                            updateDate: '2019-10-11T10:06:48Z',
                            createDate: '2019-10-11T10:06:48Z',
                            accountId: 27070,
                            postingGroupId: 24213
                        },
                        {
                            network: 'ok',
                            name: 'Аннинская сельская библиотека',
                            networkId: '56117699215549',
                            updateDate: '2019-10-11T10:06:45Z',
                            createDate: '2019-10-11T10:06:45Z',
                            accountId: 27070,
                            postingGroupId: 24212
                        },
                        {
                            network: 'ok',
                            name: 'МБУК "МЦБС Петушинского района"',
                            networkId: '52364109611148',
                            updateDate: '2019-08-27T08:34:36Z',
                            createDate: '2017-07-04T12:05:15Z',
                            accountId: 15687,
                            postingGroupId: 13580
                        },
                        {
                            network: 'ok',
                            name: 'Пахомовская библиотека',
                            networkId: '61145869451299',
                            updateDate: '2019-10-04T08:06:00Z',
                            createDate: '2019-10-04T08:06:00Z',
                            accountId: 26953,
                            postingGroupId: 24075
                        },
                        {
                            network: 'ok',
                            name: 'غغغغغ Ęlena Prokhorovaۼۼۼۼ',
                            networkId: '442617108876',
                            isPersonal: true,
                            updateDate: '2019-10-02T19:32:12Z',
                            createDate: '2019-10-02T19:32:12Z',
                            accountId: 15687,
                            postingGroupId: 24060
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '10:00:00', to: '18:00:00' },
                    '2': { from: '10:00:00', to: '18:00:00' },
                    '3': { from: '10:00:00', to: '18:00:00' },
                    '4': { from: '10:00:00', to: '18:00:00' },
                    '5': { from: '10:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Пекшинская сельская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.944Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e0',
        hash: '2019-09-27T07:59:15Z',
        nativeId: '39975',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39975',
                category: 'place',
                createDate: '2019-09-24T07:24:48Z',
                updateDate: '2019-09-27T07:59:15Z'
            },
            general: {
                id: 39975,
                name: 'Омутищенская сельская библиотека',
                description:
                    '<p>В 1927 году по инициативе местных комсомольцев при клубе была организована библиотека, которая состояла из книг, собранных на добровольные пожертвования жителей. </p><p>Колхоз выписывал для библиотеки газеты, приложение к ним. </p><p>Во время войны библиотека работала с 8 утра и до 8 вечера. С утра уходили на поля – читать сводки с фронта, помогать колхозу. </p><p>С 1932 года библиотека занимает небольшое помещение в деревянном доме. Книжный фонд составляет 6 033 экземпляра книг и журналов. Число читателей составляет 97 человек.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Набережная,д 90',
                    comment: '',
                    fiasHouseId: '54481298-fcf8-46a3-ae9e-cb494137136c',
                    fiasStreetId: '03f3a0f4-27fd-4092-a7ae-8454ea193c85',
                    fiasSettlementId: 'e23f7bef-5810-4686-8ff5-e320cf5862ef',
                    fiasAreaId: '4265d25d-8f14-4f0a-9c1f-9ad765c6c918',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Петушинский,д Старые Омутищи,ул Набережная,д 90',
                    mapPosition: { type: 'Point', coordinates: [39.322458000000005, 55.899103000000004] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40752/omutishenskaya-selskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/544e892c100ea5b0ca655e3591a761e2.jpeg',
                        title: 'IMG_1606.JPG'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/6fed797a0969b8a59a86c2202dac828f.jpeg',
                    title: 'IMG_1597.JPG'
                },
                localeIds: [536, 180, 1],
                locale: {
                    name: 'Петушинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'petushinskiy-rayon',
                    id: 536
                },
                organization: {
                    id: 18666,
                    name: 'МБУК «Межпоселенческая централизованная библиотечная система Петушинского района»',
                    inn: '3321027063',
                    type: 'mincult',
                    address: {
                        street: 'ул Строителей,д 12',
                        comment: '',
                        fiasHouseId: '153dd5cd-ea7a-43c9-b6f2-02e927b568ce',
                        fiasStreetId: 'dcba352a-94e0-439c-91ff-392f9338f633',
                        fiasCityId: 'b7ba6bfc-4b0c-4b05-88bd-267d50b36850',
                        fiasAreaId: '4265d25d-8f14-4f0a-9c1f-9ad765c6c918',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Петушинский,г Петушки,ул Строителей,д 12'
                    },
                    subordinationIds: [536, 180, 1],
                    subordination: {
                        name: 'Петушинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petushinskiy-rayon',
                        id: 536
                    },
                    localeIds: [536, 180, 1],
                    locale: {
                        name: 'Петушинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'petushinskiy-rayon',
                        id: 536
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Аннинская Сельская-Библиотека',
                            networkId: '556479238',
                            isPersonal: true,
                            updateDate: '2019-10-10T07:42:45Z',
                            createDate: '2019-10-10T07:42:45Z',
                            accountId: 27018,
                            postingGroupId: 24153
                        },
                        {
                            network: 'vk',
                            name: 'Караваевская сельская библиотека МБУК "МЦБС Пету',
                            networkId: '147183014',
                            updateDate: '2019-09-22T11:04:50Z',
                            createDate: '2019-09-22T11:04:50Z',
                            accountId: 26853,
                            postingGroupId: 23975
                        },
                        {
                            network: 'vk',
                            name: 'Костинская сельская библиотека',
                            networkId: '186443201',
                            updateDate: '2019-10-11T09:18:50Z',
                            createDate: '2019-10-11T09:18:50Z',
                            accountId: 27065,
                            postingGroupId: 24208
                        },
                        {
                            network: 'vk',
                            name: 'МБУК "МЦБС Петушинского района"',
                            networkId: '117833536',
                            updateDate: '2017-07-04T12:08:49Z',
                            createDate: '2017-07-04T12:08:49Z',
                            accountId: 15692,
                            postingGroupId: 13581
                        },
                        {
                            network: 'vk',
                            name: 'Нагорная сельская библиотека',
                            networkId: '131516604',
                            updateDate: '2019-10-04T07:38:03Z',
                            createDate: '2019-10-04T07:38:03Z',
                            accountId: 26949,
                            postingGroupId: 24072
                        },
                        {
                            network: 'vk',
                            name: 'Пахомовская сельская библиотека',
                            networkId: '166589326',
                            updateDate: '2019-10-04T08:33:47Z',
                            createDate: '2019-10-04T08:33:47Z',
                            accountId: 26954,
                            postingGroupId: 24076
                        },
                        {
                            network: 'vk',
                            name: 'Светлана Длэц',
                            networkId: '328390229',
                            isPersonal: true,
                            updateDate: '2019-10-04T08:52:16Z',
                            createDate: '2019-10-04T08:52:16Z',
                            accountId: 26955,
                            postingGroupId: 24077
                        },
                        {
                            network: 'vk',
                            name: 'Читай, Петушинский район',
                            networkId: '86380164',
                            updateDate: '2019-09-03T13:03:40Z',
                            createDate: '2019-08-29T08:06:41Z',
                            accountId: 15692,
                            postingGroupId: 23716
                        },
                        {
                            network: 'twitter',
                            name: 'МБУК «МЦБС Петушинского района»',
                            networkId: '1166647943866310661',
                            isPersonal: true,
                            updateDate: '2019-08-29T13:58:22Z',
                            createDate: '2019-08-29T13:58:22Z',
                            accountId: 26609,
                            postingGroupId: 23729
                        },
                        {
                            network: 'twitter',
                            name: 'Читай, Петушинский район',
                            networkId: '1168896224608567296',
                            isPersonal: true,
                            updateDate: '2019-09-03T14:47:14Z',
                            createDate: '2019-09-03T14:47:14Z',
                            accountId: 26652,
                            postingGroupId: 23770
                        },
                        {
                            network: 'ok',
                            name: 'Аннинская сельская библиотека',
                            networkId: '578852312253',
                            isPersonal: true,
                            updateDate: '2019-10-11T10:06:48Z',
                            createDate: '2019-10-11T10:06:48Z',
                            accountId: 27070,
                            postingGroupId: 24213
                        },
                        {
                            network: 'ok',
                            name: 'Аннинская сельская библиотека',
                            networkId: '56117699215549',
                            updateDate: '2019-10-11T10:06:45Z',
                            createDate: '2019-10-11T10:06:45Z',
                            accountId: 27070,
                            postingGroupId: 24212
                        },
                        {
                            network: 'ok',
                            name: 'МБУК "МЦБС Петушинского района"',
                            networkId: '52364109611148',
                            updateDate: '2019-08-27T08:34:36Z',
                            createDate: '2017-07-04T12:05:15Z',
                            accountId: 15687,
                            postingGroupId: 13580
                        },
                        {
                            network: 'ok',
                            name: 'Пахомовская библиотека',
                            networkId: '61145869451299',
                            updateDate: '2019-10-04T08:06:00Z',
                            createDate: '2019-10-04T08:06:00Z',
                            accountId: 26953,
                            postingGroupId: 24075
                        },
                        {
                            network: 'ok',
                            name: 'غغغغغ Ęlena Prokhorovaۼۼۼۼ',
                            networkId: '442617108876',
                            isPersonal: true,
                            updateDate: '2019-10-02T19:32:12Z',
                            createDate: '2019-10-02T19:32:12Z',
                            accountId: 15687,
                            postingGroupId: 24060
                        }
                    ]
                },
                recommendations: [
                    { name: 'Петушинский район', timezone: 'Europe/Moscow', sysName: 'petushinskiy-rayon', id: 536 }
                ],
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '10:00:00', to: '14:30:00' },
                    '2': { from: '10:00:00', to: '14:30:00' },
                    '4': { from: '10:00:00', to: '14:30:00' },
                    '5': { from: '10:00:00', to: '14:30:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Омутищенская сельская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.949Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e1',
        hash: '2019-09-27T06:33:13Z',
        nativeId: '39969',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39969',
                category: 'place',
                createDate: '2019-09-23T00:14:40Z',
                updateDate: '2019-09-27T06:33:13Z'
            },
            general: {
                id: 39969,
                name: 'Библиотека № 1 им. Н. А. Островского',
                description:
                    '<p> Библиотека № 1 им. Н. А. Островского – одна из старейших в г. Чите. Она основана в 1939 году и располагалась в самом центре города – в здании Дома пионеров на перекрестке улиц им. Ленина и им. П. Осипенко. Библиотека обслуживала детей и взрослых всего города. В 1959 г., получив новое помещение – часть первого этажа в благоустроенном жилом доме на ул. им. Н. Островского, 6, обрела статус детской библиотеки. Для юных читателей здесь открылись читальный зал и 2 абонемента: для учащихся младшего школьного возраста и для старшеклассников. В 1961 г. библиотеке присвоено имя писателя Н. Островского. </p><p>Сегодня библиотечный фонд – 21 371 экземпляр, ежегодно библиотекой пользуются около 2 000 читателей, библиотека выдает им около 41 000 экземпляров документов. Библиотека пользуется большим спросом у учащихся школ №№ 1, 12, 49, для которых работники библиотеки проводят диспуты, познавательные часы на различные темы, обсуждения книг, литературные вечера. С удовольствием и радостью посещают библиотеку воспитанники дошкольных образовательных учреждений №№ 10, 13, 18, 63, 47, 71. </p><p>Проводятся «Дни библиотеки в детском саду», организовано нестационарное обслуживание дошкольников. На познавательных часах клуба «Почемучата» и эколого-краеведческого клуба «Рюкзачок» малыши узнают много интересного об окружающем их мире, учатся любить и беречь природу родного края. </p><p>Библиотека работает как библиотека семейного чтения. Тесно сотрудничает с библиотеками госпиталя Управления пограничных войск и батальона связи. Вклад в развитие библиотеки внесли Артеменко Т. П., заслуженный работник культуры Читинской области Тимофеева Н. А., Уздяева Г. Д., Михайлова Е. В. </p><p>Заведующая – Елена Викторовна Михайлова.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Николая Островского,д 6',
                    comment: '',
                    fiasHouseId: '6ddee32e-26fd-4bf0-a0b9-312a87a761bd',
                    fiasStreetId: 'f1687379-6a70-41ac-9827-160be20ea08b',
                    fiasCityId: '2d9abaa6-85a6-4f1f-a1bd-14b76ec17d9c',
                    fiasRegionId: 'b6ba5716-eb48-401b-8443-b197c9578734',
                    fullAddress: 'край Забайкальский,г Чита,ул Николая Островского,д 6',
                    mapPosition: { type: 'Point', coordinates: [113.510715, 52.026277] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: {
                    website: 'http://cbs-chita.ru',
                    email: 'cbs_chitaostrovskogo@mail.ru',
                    phones: [
                        { value: '73022320021', comment: '' },
                        { value: '73022261545', comment: '' }
                    ]
                },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40750/biblioteka-1-im-n-a-ostrovskogo',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/871fcee3134addb1c21f02682ed3ff94.jpeg',
                        title: 'фойе 1.JPG'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/090c4984c4436f67939ea44ba1fc62de.jpeg',
                        title: 'абонемент 1.JPG'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/b85d6bbe1a6b55cc11debaacd12eb5d0.jpeg',
                        title: 'читальный зал 1.JPG'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/9c1d5e17c8eb4361ebdf3dd919146334.jpeg',
                    title: 'фасад библиотеки со стороны улицы (главный вход).JPG'
                },
                localeIds: [389, 226, 1],
                locale: { name: 'Чита', timezone: 'Asia/Yakutsk', sysName: 'chita', id: 389 },
                organization: {
                    id: 13407,
                    name: 'МБУК «Централизованная библиотечная система» г. Читы',
                    inn: '7536005860',
                    type: 'mincult',
                    address: {
                        street: 'ул Угданская,д 40',
                        fiasHouseId: 'dab6eb1b-b4e9-459c-9487-9c07d6864bd6',
                        fiasStreetId: '2343f643-87f5-485e-b887-a579d23836aa',
                        fiasCityId: '2d9abaa6-85a6-4f1f-a1bd-14b76ec17d9c',
                        fiasRegionId: 'b6ba5716-eb48-401b-8443-b197c9578734',
                        fullAddress: 'край Забайкальский,г Чита,ул Угданская,д 40'
                    },
                    subordinationIds: [389, 226, 1],
                    subordination: { name: 'Чита', timezone: 'Asia/Yakutsk', sysName: 'chita', id: 389 },
                    localeIds: [389, 226, 1],
                    locale: { name: 'Чита', timezone: 'Asia/Yakutsk', sysName: 'chita', id: 389 }
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '0': { from: '09:00:00', to: '18:00:00' },
                    '1': { from: '09:00:00', to: '18:00:00' },
                    '2': { from: '09:00:00', to: '18:00:00' },
                    '3': { from: '09:00:00', to: '18:00:00' },
                    '4': { from: '09:00:00', to: '18:00:00' },
                    '6': { from: '09:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Библиотека № 1 им. Н. А. Островского',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.951Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e3',
        hash: '2019-09-24T13:20:53Z',
        nativeId: '39961',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39961',
                category: 'place',
                createDate: '2019-09-20T06:45:48Z',
                updateDate: '2019-09-24T13:20:53Z'
            },
            general: {
                id: 39961,
                name: 'Зиновьевский сельский библиотечный филиал',
                description:
                    '<p>Филиал открыт в 1933 году как изба-читальня. В настоящее время книжный фонд составляет 5 813 экземпляров, количество пользователей – 110, книговыдача – 4 600 экземпляров. Библиотека компьютеризирована. Зиновьевская сельская библиотека – единственное культурно-досуговое учреждение в селе. В библиотеку тянутся взрослые и дети. </p><p> Проводится целенаправленная работа по продвижению книги. Особое внимание уделяется формированию любви к малой родине, духовно-нравственным традициям родного села. В библиотеке создана летопись села Зиновьево, собран богатый краеведческий материал по истории сельского храма, о достойных людях, живших в разные времена и оставивших заметный след в истории малой родины. </p><p>Читатели библиотеки получают доступ к сети Интернет, электронному каталогу, порталу «Государственные услуги РФ». Библиотека осуществляет онлайн-продление литературы, использует электронную доставку документов. </p>',
                status: 'accepted',
                address: {
                    street: 'ул Пятая,д 4',
                    comment: '',
                    fiasHouseId: 'f49b4f5e-53e3-4fa8-9334-ff56a987800a',
                    fiasStreetId: '4d1334ed-a869-46dd-a0f3-68bd016ced15',
                    fiasSettlementId: '22111598-d1a1-497e-bb30-74a30ead17ae',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,с Зиновьево,ул Пятая,д 4',
                    mapPosition: { type: 'Point', coordinates: [39.418065999999996, 56.154464999999995] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'belyshewa.natali@yandex.ru', phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40744/zinovevskii-selskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/f2a0f2a22b69046837c7d6d5c79337ac.jpeg',
                    title: 'DSCN6302.JPG'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' },
                    { id: 406, name: 'Краеведение', sysName: 'kraevedenie' }
                ],
                workingSchedule: {
                    '2': { from: '09:00:00', to: '16:30:00' },
                    '3': { from: '09:00:00', to: '16:30:00' },
                    '4': { from: '09:00:00', to: '16:30:00' },
                    '5': { from: '09:00:00', to: '16:30:00' },
                    '6': { from: '09:00:00', to: '16:30:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Зиновьевский сельский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.954Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e4',
        hash: '2019-10-01T14:01:36Z',
        nativeId: '39960',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39960',
                category: 'place',
                createDate: '2019-09-20T06:32:01Z',
                updateDate: '2019-10-01T14:01:36Z'
            },
            general: {
                id: 39960,
                name: 'Фроловская сельская библиотека',
                description:
                    '<p>Библиотека Фроловского сельского поселения основана в 1953 году, находилась в старом помещении школы, затем переехала в здание администрации и уже больше 20 лет располагается в здании Дома культуры. </p><p>Структура библиотеки: основной отдел в с. Фролы и Симакинский библиотечный пункт в д. Жебреи. </p><p>Для читателей всегда доступны периодические издания – журналы для мам: «Мой ребенок», «Домашний очаг», журналы для садоводов: «Приусадебное хозяйство», «САД», журналы для модниц, для тех, кто обеспокоен своим здоровьем. </p><p>Детский фонд составляет 5 416 экземпляров. Весь фонд библиотеки составляет 11 430 экземпляров. В библиотеке действуют постоянные выставки и тематические полки, которые периодически обновляются и пополняются новой литературой. Отдельно оформлен «Семейный зал» – это небольшой уголок для детей от года до шести и их родителей. Основной фонд для взрослых, в библиотеке 5 042 экземпляра художественной литературы. В библиотеке ведется систематический каталог, в 2015 году закуплена программа для электронного каталога, в который внесено 1 222 документа. К каждому празднику оформляется выставка «Тема дня». В библиотеке проходят экскурсии для дошколят.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Садовая,д 14',
                    comment: 'через ул. Героев Хасана, маршрут №№ 823, 121 до остановки «Фролы»',
                    fiasHouseId: 'aa04c628-aee3-4b74-85ae-183c18066aa4',
                    fiasStreetId: 'd3372a28-6bdf-44a4-842f-8dc9f299a931',
                    fiasSettlementId: '70387696-c15b-4df2-8e26-9c93b3a11c7e',
                    fiasAreaId: '7dd380b3-ce33-4280-934f-a4265a07803b',
                    fiasRegionId: '4f8b1a21-e4bb-422f-9087-d3cbf4bebc14',
                    fullAddress: 'край Пермский,р-н Пермский,с Фролы,ул Садовая,д 14',
                    mapPosition: { type: 'Point', coordinates: [56.272527873516076, 57.91894221685171] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: {
                    website: 'http://dkfrolovsky.ru',
                    email: 'evseenkova57@mail.ru',
                    phones: [{ value: '73422998380', comment: '' }]
                },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40765/frolovskaya-selskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/6d07b37e7c711f9601678ed16e52f898.jpeg',
                        title: 'h2dQbPlhg9Y.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/8ee1c345c06ad7f60b674f7220744a1d.jpeg',
                        title: 'MJM-g4UY3JU.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/db11dd97b062e1469af9189c90a7ad67.jpeg',
                        title: '5ZcinVQTgQA.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/9e07782e9eb86faee5a851a442a34e09.jpeg',
                        title: 'OazbrgeUc9M.jpg'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/9e18a67e8518c63ef514e6c379d8b9a0.jpeg',
                    title: '7b76ced623069a7c31899c3233a555fa_w2048_h2048.jpeg'
                },
                localeIds: [1552, 209, 1],
                locale: { name: 'Пермский район', timezone: 'Asia/Yekaterinburg', sysName: 'permskiy-rayon', id: 1552 },
                organization: {
                    id: 11207,
                    name: 'МУ «Фроловский сельский дом культуры»',
                    inn: '5948019636',
                    type: 'mincult',
                    address: {
                        street: 'ул Садовая,д 14',
                        comment: '',
                        fiasHouseId: 'aa04c628-aee3-4b74-85ae-183c18066aa4',
                        fiasStreetId: 'd3372a28-6bdf-44a4-842f-8dc9f299a931',
                        fiasSettlementId: '70387696-c15b-4df2-8e26-9c93b3a11c7e',
                        fiasAreaId: '7dd380b3-ce33-4280-934f-a4265a07803b',
                        fiasRegionId: '4f8b1a21-e4bb-422f-9087-d3cbf4bebc14',
                        fullAddress: 'край Пермский,р-н Пермский,с Фролы,ул Садовая,д 14'
                    },
                    subordinationIds: [1552, 209, 1],
                    subordination: {
                        name: 'Пермский район',
                        timezone: 'Asia/Yekaterinburg',
                        sysName: 'permskiy-rayon',
                        id: 1552
                    },
                    localeIds: [1552, 209, 1],
                    locale: {
                        name: 'Пермский район',
                        timezone: 'Asia/Yekaterinburg',
                        sysName: 'permskiy-rayon',
                        id: 1552
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Фроловский Дом Культуры (342) 299 81 18',
                            networkId: '32699547',
                            updateDate: '2019-09-24T10:03:27Z',
                            createDate: '2019-09-24T10:03:27Z',
                            accountId: 26875,
                            postingGroupId: 23998
                        }
                    ]
                },
                recommendations: [
                    { name: 'Пермский район', timezone: 'Asia/Yekaterinburg', sysName: 'permskiy-rayon', id: 1552 }
                ],
                tags: [
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' },
                    { id: 596, name: 'Доступная среда', sysName: 'dostupnaya-sreda' }
                ],
                workingSchedule: {
                    '1': { from: '12:00:00', to: '19:00:00' },
                    '2': { from: '12:00:00', to: '19:00:00' },
                    '3': { from: '12:00:00', to: '19:00:00' },
                    '4': { from: '12:00:00', to: '19:00:00' },
                    '5': { from: '12:00:00', to: '17:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Фроловская сельская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.957Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e5',
        hash: '2019-09-24T13:13:32Z',
        nativeId: '39959',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39959',
                category: 'place',
                createDate: '2019-09-20T05:42:17Z',
                updateDate: '2019-09-24T13:13:32Z'
            },
            general: {
                id: 39959,
                name: 'Золотухинский сельский библиотечный филиал',
                description:
                    '<p>Открыт в 1968 году. Библиотека сегодня:</p><ul><li>книжный фонд – 11 131 экземпляр;</li><li> число пользователей за год – 255 ;</li><li> общее число посещений – 3 902;</li><li>в год выдается более 5 000 экземпляров литературы.</li></ul><p> Золотухинский сельский библиотечный филиал – это центр общения и досуга как для детей и подростков, так и для взрослого населения поселка. Чтобы идти в ногу со временем, заведующая библиотекой постоянно ведет поиск эффективных форм работы:&nbsp;познавательных, занимательных, зрелищных. </p><p>Успешно осваиваются и новые технологии: компьютерная техника помогает как библиотекарю, так и пользователям. Заведующая библиотекой старается развивать профессиональное сотрудничество с коллегами из района области: знакомится с лучшим библиотечным опытом, новыми проектами. Это помогает решать возникающие проблемы, находить партнеров для успешной деятельности библиотеки, развивать сотрудничество в местном сообществе. </p>',
                status: 'accepted',
                address: {
                    street: 'ул Тринадцатая,д 2',
                    comment: '',
                    fiasHouseId: 'd60cfe45-1824-4daf-b7e7-1db41c5f8778',
                    fiasStreetId: '5a195ca3-1fdb-4b80-9145-cc19d610f4f0',
                    fiasSettlementId: '35855d7f-0710-4fa0-884a-1e19753056bb',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,п Золотуха,ул Тринадцатая,д 2',
                    mapPosition: { type: 'Point', coordinates: [39.349092999999996, 56.427786999999995] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'zolotbib@gmail.com', phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40745/zolotukhinskii-selskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/17efc2161fb52b649b32edd654a07e9f.jpeg',
                        title: 'IMG-20190919-WA0007.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/f2c79924d8d9088e7fbe26629713fff2.jpeg',
                        title: 'IMG-20190919-WA0006.jpg'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/372b0f0d2396334943120f38e62ca1ab.jpeg',
                    title: 'IMG-20190919-WA0000.jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '11:00:00', to: '19:00:00' },
                    '2': { from: '11:00:00', to: '19:00:00' },
                    '3': { from: '11:00:00', to: '19:00:00' },
                    '4': { from: '11:00:00', to: '19:00:00' },
                    '5': { from: '11:00:00', to: '19:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Золотухинский сельский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.959Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e6',
        hash: '2019-09-24T13:11:23Z',
        nativeId: '39958',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39958',
                category: 'place',
                createDate: '2019-09-20T05:34:49Z',
                updateDate: '2019-09-24T13:11:23Z'
            },
            general: {
                id: 39958,
                name: 'Беречинский сельский библиотечный филиал',
                description:
                    '<p>Открыт в 1957 году. Библиотека сегодня: </p><ul><li>фонд – 9 847 экземпляров;</li><li>число пользователей – 196;</li><li>количество посещений за год – 2 061;</li><li>за год проводится более 50 мероприятий. </li></ul><p>Библиотека компьютеризирована, читательский контингент самый разнообразный: дети, взрослые, пенсионеры. </p><p>Беречинский сельский библиотечный филиал – единственное культурное учреждение села Беречино. Библиотека стремится приобщить жителей села к чтению, книге, сделать ее любимым и посещаемым местом. Библиотека предоставляет пользователям доступ к ресурсам сети Интернет, к СПС «КонсультантПлюс», электронному каталогу, порталу «Государственные услуги РФ», осуществляет онлайн-продление литературы, электронную доставку документов. </p>',
                status: 'accepted',
                address: {
                    street: 'д 107',
                    comment: '',
                    fiasHouseId: 'c412d7bb-4940-47f7-9c2d-219bb2967b8d',
                    fiasSettlementId: 'c937be64-dc7b-43bf-9f2d-6ea161d87c97',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,с Беречино,д 107',
                    mapPosition: { type: 'Point', coordinates: [39.399201, 56.26614200000001] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'bercb@mail.ru', phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40746/berechinskii-selskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/6b12945d281062e4d8bcea0116ac826e.jpeg',
                        title: 'IMG-20190919-WA0023.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/e67ba1d13f3c0d114909358498deaf72.jpeg',
                        title: 'IMG-20190919-WA0024.jpg'
                    },
                    {
                        url: 'https://all.culture.ru/uploads/97a1000b7f86ca1cf0e7f547a2627368.jpeg',
                        title: 'IMG-20190919-WA0018.jpg'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/99b939930f33893b6df2ba2145dc420a.jpeg',
                    title: 'IMG-20190919-WA0028.jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                workingSchedule: {
                    '1': { from: '09:30:00', to: '17:30:00' },
                    '2': { from: '09:30:00', to: '17:30:00' },
                    '3': { from: '09:30:00', to: '17:30:00' },
                    '4': { from: '09:30:00', to: '17:30:00' },
                    '5': { from: '09:30:00', to: '17:30:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Беречинский сельский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.960Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e7',
        hash: '2019-09-24T12:45:40Z',
        nativeId: '39951',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39951',
                category: 'place',
                createDate: '2019-09-19T07:46:07Z',
                updateDate: '2019-09-24T12:45:40Z'
            },
            general: {
                id: 39951,
                name: 'Пролетарская библиотека',
                description:
                    'Пролетарская библиотека – филиал № 19 основана в 1965 году, фонд библиотеки составляет 11 033 экземпляра. Библиотека обслуживает все слои населения: людей с ограниченными возможностями, детей, молодежь, взрослых. Тесно сотрудничает со школой. При библиотеке работает клуб «Креативчики», действует детская зона «Библиопродленки». Используется комплекс форм индивидуальной и массовой работы.',
                status: 'accepted',
                address: {
                    street: 'ул Мира,д 14',
                    comment: '',
                    fiasStreetId: '8a093c4b-a36e-4268-a9a5-c0e9c8761fc7',
                    fiasSettlementId: 'a3502098-9c7e-4537-98b4-da270712ddb6',
                    fiasAreaId: '03c63d2b-721b-4da1-b4af-9380115d9c94',
                    fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                    fullAddress: 'край Ставропольский,р-н Курский,х Пролетарский,ул Мира,д 14',
                    mapPosition: { type: 'Point', coordinates: [44.123666, 44.009714] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'mturlucheva@mail.ru', phones: [{ value: '78796470425', comment: '' }] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40738/proletarskaya-biblioteka',
                        serviceName: 'Культура.рф'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/b352e5513189be939bbf607e2b12b805.jpeg',
                    title: 'SAM_9094.jpg'
                },
                localeIds: [2747, 176, 1],
                locale: { name: 'Курский район', timezone: 'Europe/Moscow', sysName: 'kurskiy-rayon', id: 2747 },
                organization: {
                    id: 15397,
                    name: 'МУ «Межпоселенческая центральная библиотека» Курского МР Ставропольского края',
                    inn: '2612007951',
                    type: 'mincult',
                    address: {
                        street: 'ул Советская,д 17',
                        comment: '',
                        fiasStreetId: '754587bb-ef34-45ed-a685-a05c5a02eee1',
                        fiasSettlementId: '34d2f57d-673f-4870-b4da-108d3b4981d3',
                        fiasAreaId: '03c63d2b-721b-4da1-b4af-9380115d9c94',
                        fiasRegionId: '327a060b-878c-4fb4-8dc4-d5595871a3d8',
                        fullAddress: 'край Ставропольский,р-н Курский,ст-ца Курская,ул Советская,д 17'
                    },
                    subordinationIds: [2747, 176, 1],
                    subordination: {
                        name: 'Курский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kurskiy-rayon',
                        id: 2747
                    },
                    localeIds: [2747, 176, 1],
                    locale: { name: 'Курский район', timezone: 'Europe/Moscow', sysName: 'kurskiy-rayon', id: 2747 },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Балтийская сельская библиотека',
                            networkId: '119887643',
                            updateDate: '2019-10-10T11:57:00Z',
                            createDate: '2019-10-10T11:57:00Z',
                            accountId: 27029,
                            postingGroupId: 24177
                        },
                        {
                            network: 'vk',
                            name: 'Галина Миронова',
                            networkId: '180721243',
                            isPersonal: true,
                            updateDate: '2018-07-23T10:08:47Z',
                            createDate: '2018-07-23T10:08:47Z',
                            accountId: 20792,
                            postingGroupId: 19459
                        },
                        {
                            network: 'vk',
                            name: 'Курская Межпоселенческая Библиотека Ставрополье',
                            networkId: '173229082',
                            updateDate: '2018-11-01T10:51:00Z',
                            createDate: '2018-11-01T10:51:00Z',
                            accountId: 22669,
                            postingGroupId: 20417
                        },
                        {
                            network: 'vk',
                            name: 'Курская Межпоселенческая-Библиотека',
                            networkId: '443017735',
                            isPersonal: true,
                            updateDate: '2018-11-01T10:52:43Z',
                            createDate: '2018-11-01T10:52:43Z',
                            accountId: 22669,
                            postingGroupId: 20418
                        },
                        {
                            network: 'vk',
                            name: 'Курская-Районная Детская-Библиотека',
                            networkId: '325965834',
                            isPersonal: true,
                            updateDate: '2019-08-01T07:56:21Z',
                            createDate: '2019-08-01T07:56:21Z',
                            accountId: 26362,
                            postingGroupId: 23463
                        },
                        {
                            network: 'vk',
                            name: 'Русская библиотека - филиал №9',
                            networkId: '158235522',
                            updateDate: '2019-10-01T09:20:18Z',
                            createDate: '2019-10-01T09:20:18Z',
                            accountId: 26917,
                            postingGroupId: 24047
                        }
                    ]
                },
                recommendations: [
                    { name: 'Курский район', timezone: 'Europe/Moscow', sysName: 'kurskiy-rayon', id: 2747 }
                ],
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '09:00:00', to: '17:00:00' },
                    '2': { from: '09:00:00', to: '17:00:00' },
                    '3': { from: '09:00:00', to: '17:00:00' },
                    '4': { from: '09:00:00', to: '17:00:00' },
                    '5': { from: '09:00:00', to: '17:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Пролетарская библиотека',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.962Z'
    },
    {
        _id: '5da833d7628b0e30ade6b2e9',
        hash: '2019-09-20T08:41:13Z',
        nativeId: '39941',
        activated: '2019-10-17T09:26:47.121Z',
        data: {
            info: {
                path: '/cdm/v2/libraries/39941',
                category: 'place',
                createDate: '2019-09-18T06:02:17Z',
                updateDate: '2019-09-20T08:41:13Z'
            },
            general: {
                id: 39941,
                name: 'Есиплевский библиотечный филиал',
                description:
                    '<p>Функционирует с 1954 года. Книжный фонд библиотеки насчитывает 11 879 экземпляров. Ежегодно библиотеку посещают 268 пользователей, количество посещений – 5 216, проводится более 60 мероприятий в год. Библиотека компьютеризирована. </p><p><span>Есиплевский</span> сельский библиотечный филиал сегодня – это <span>информационно-досуговый</span> центр, где сочетаются традиционные формы библиотечной деятельности с инновационными. Юные книгочеи пользуются здесь особым вниманием и заботой. К их услугам отличный книжный фонд, устраиваются различные мероприятия: конкурсы, викторины, презентации новинок, игровые программы и праздники. </p><p>В библиотеке осваиваются новые технологии: компьютер помогает в работе библиотекарю и читателям. В селе <span>Есиплево</span> сложилась особая межкультурная среда – здесь живут представители разных национальностей. Библиотечное обслуживание происходит с обязательным соблюдением принципов толерантности, уважением гражданских свобод, диалога и равного доступа к информации.</p>',
                status: 'accepted',
                address: {
                    street: 'ул Коллективная,д 4',
                    comment: '',
                    fiasHouseId: '8c6a6dd5-302c-4a94-b91e-00df8a84de79',
                    fiasStreetId: '72730b94-9d81-4883-9c1e-a4d82f3532af',
                    fiasSettlementId: '6add804b-a5cd-48b6-a79f-013d73acfc4d',
                    fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                    fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                    fullAddress: 'обл Владимирская,р-н Кольчугинский,с Есиплево,ул Коллективная,д 4',
                    mapPosition: { type: 'Point', coordinates: [39.542482, 56.302473] }
                },
                category: { name: 'Библиотеки', sysName: 'biblioteki' },
                contacts: { email: 'esipbib@gmail.com', phones: [] },
                externalInfo: [
                    {
                        url: 'https://www.culture.ru/institutes/40714/esiplevskii-bibliotechnyi-filial',
                        serviceName: 'Культура.рф'
                    }
                ],
                gallery: [
                    {
                        url: 'https://all.culture.ru/uploads/42e6c2f53f497e6e7825c8e77a6a0a00.jpeg',
                        title: 'IMG-20190917-WA0001.jpg'
                    }
                ],
                image: {
                    url: 'https://all.culture.ru/uploads/23da1ae488b5b9abd4a0ef26afb309c0.jpeg',
                    title: 'IMG-20190917-WA0011.jpg'
                },
                localeIds: [533, 180, 1],
                locale: {
                    name: 'Кольчугинский район',
                    timezone: 'Europe/Moscow',
                    sysName: 'kolchuginskiy-rayon',
                    id: 533
                },
                organization: {
                    id: 13519,
                    name: 'МБУК Кольчугинского района «Межпоселенческая центральная библиотека»',
                    inn: '3318004186',
                    type: 'mincult',
                    address: {
                        street: 'ул Ленина,д 4',
                        comment: '',
                        fiasHouseId: 'e60420a8-05d6-4bc8-a9a4-8fa7eb699248',
                        fiasStreetId: '63b9fbe5-a4b6-4355-9856-e67b9fa0cccf',
                        fiasCityId: '8f9faad4-ff93-471d-b0c0-c8e5c0162dee',
                        fiasAreaId: '1309495e-71a0-4da2-897c-a6ee5f006e04',
                        fiasRegionId: 'b8837188-39ee-4ff9-bc91-fcc9ed451bb3',
                        fullAddress: 'обл Владимирская,р-н Кольчугинский,г Кольчугино,ул Ленина,д 4'
                    },
                    subordinationIds: [533, 180, 1],
                    subordination: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    localeIds: [533, 180, 1],
                    locale: {
                        name: 'Кольчугинский район',
                        timezone: 'Europe/Moscow',
                        sysName: 'kolchuginskiy-rayon',
                        id: 533
                    },
                    socialGroups: [
                        {
                            network: 'vk',
                            name: 'Юлия Аринина',
                            networkId: '395601682',
                            isPersonal: true,
                            updateDate: '2019-07-01T07:31:50Z',
                            createDate: '2019-07-01T07:31:50Z',
                            accountId: 25943,
                            postingGroupId: 23144
                        },
                        {
                            network: 'ok',
                            name: 'Кольчугинская Центральная библиотека',
                            networkId: '54266709803129',
                            updateDate: '2019-09-17T06:47:25Z',
                            createDate: '2019-09-17T06:47:25Z',
                            accountId: 26800,
                            postingGroupId: 23923
                        }
                    ]
                },
                tags: [
                    { id: 28, name: 'Литература', sysName: 'literatura' },
                    { id: 39, name: 'Для детей', sysName: 'dlya-detey' },
                    { id: 317, name: 'Для молодежи', sysName: 'dlya-molodezhi' },
                    { id: 392, name: 'Книги', sysName: 'knigi' }
                ],
                workingSchedule: {
                    '1': { from: '10:00:00', to: '18:00:00' },
                    '2': { from: '10:00:00', to: '18:00:00' },
                    '3': { from: '10:00:00', to: '18:00:00' },
                    '4': { from: '10:00:00', to: '18:00:00' },
                    '5': { from: '10:00:00', to: '18:00:00' }
                },
                ticketReport: []
            }
        },
        status: 2,
        errorFields: [
            {
                keyword: 'type',
                dataPath: '.general.address.mapPosition.coordinates[0]',
                schemaPath:
                    '#/properties/general/properties/address/properties/mapPosition/properties/coordinates/items/type',
                params: { type: 'array' },
                message: 'should be array'
            }
        ],
        updateSession: '5da8336954ec2330be054c2a',
        odSetVersions: [
            '5da8336954ec2330be054c29',
            '5dd099c86ee3b330ffe70f62',
            '5e21038548ede0435f93ad65',
            '5e49da2379519112ffd7c7ba',
            '5e716fa02946bb5c7c2b2dad',
            '5e9a4c545d8e423c99dc18cc',
            '5ec3246d3d626a65e8e53f43',
            '5eeaba7fd621e4465c6a45d8',
            '5f139607a774e43e1c361c58',
            '5f3b2318a774e43e1c36201b',
            '5f640167ed42ed17bca5c5db',
            '5f8cdccf84b4815a1d484b3c',
            '5fb469de84b4815a1d484e1b',
            '5fdd486d84b4815a1d485139',
            '6004d57d84b4815a1d48547d'
        ],
        odSchema: '5da83264d0ed0130a8350077',
        dataset: '5774fbc7f2485ab25d8b7f35',
        nativeName: 'Есиплевский библиотечный филиал',
        created: '2019-10-17T09:26:47.182Z',
        modified: '2021-01-18T00:29:30.967Z'
    }
];
