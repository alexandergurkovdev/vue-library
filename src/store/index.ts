import Vue from 'vue';
import Vuex from 'vuex';
import { timeout } from '@/utils';
import data from './mocks/data';
import { Libraries } from '@/types';

Vue.use(Vuex);

export interface State {
    libraries: Libraries[];
    loading: boolean;
    active: Libraries[];
    itemsShow: number;
    term: string;
}

export default new Vuex.Store({
    state: {
        libraries: [] as any[],
        loading: false,
        active: [] as any[],
        itemsShow: 12,
        term: ''
    },
    actions: {
        async getBooks({ commit }) {
            commit('setLoading', true);
            await timeout(1000);
            commit('setBooks', data);
            commit('setLoading', false);
        },
        async loadMore({ commit }) {
            commit('setLoading', true);
            await timeout(500);
            commit('setItems');
            commit('setLoading', false);
        },
        async fetchActiveLibrary({ commit }, id: string) {
            commit('setLoading', true);
            await timeout(1000);
            const book = data.filter(item => item._id === id);
            commit('setActive', book);
            commit('setLoading', false);
        }
    },
    mutations: {
        setLoading(state, payload: boolean) {
            state.loading = payload;
        },
        setBooks(state, payload: Libraries[]) {
            state.libraries = payload;
        },
        setItems(state, payload: number = 4) {
            state.itemsShow += payload;
        },
        setTerm(state, payload: string) {
            state.term = payload;
        },
        setActive(state, payload: Libraries[]) {
            state.active = payload;
        }
    },
    getters: {
        libraries(state: State) {
            const checkTerm = (item: { data: { general: { name: string } } }, term: string) =>
                item.data.general.name.toLowerCase().indexOf(term.toLowerCase()) > -1;
            return state.libraries.filter(item => checkTerm(item, state.term)).slice(0, state.itemsShow);
        },
        librariesLength(state: State) {
            return state.libraries.length;
        },
        itemsShow(state: State) {
            return state.itemsShow;
        },
        loading(state: State) {
            return state.loading;
        },
        active(state: State) {
            return state.active[0];
        }
    }
});
